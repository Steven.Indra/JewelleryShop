package com.project.jewelleryshop.transaction;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.project.jewelleryshop.item.Item;

import javax.persistence.*;


import java.util.Date;

@Entity
@Table(name="item_transaction")
@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@id")
public class Transaction {

    public Transaction(){}


	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public long getItemSoldPrice() {
		return itemSoldPrice;
	}

	public void setItemSoldPrice(long itemSoldPrice) {
		this.itemSoldPrice = itemSoldPrice;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public String getTransactionPIC() {
		return transactionPIC;
	}

	public void setTransactionPIC(String transactionPIC) {
		this.transactionPIC = transactionPIC;
	}

	@Column(name = "transaction_id", nullable = false)
    private String transactionId;
    @Column(name = "transaction_date", nullable = false)
    private Date transactionDate;
    @Column(name = "item_sold_price", nullable = false)
    private long itemSoldPrice;
    @Id
    @Column(name = "item_id", nullable = false)
    private String itemId;
    @Column(name = "person_in_charge", nullable = false)
    private String transactionPIC;
    @OneToOne
    @PrimaryKeyJoinColumn
	private Item item;
}
