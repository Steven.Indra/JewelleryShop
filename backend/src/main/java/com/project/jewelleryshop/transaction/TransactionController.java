package com.project.jewelleryshop.transaction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TransactionController {

    @Autowired
    private TransactionRepository repository;

    @RequestMapping(value = "getTransactions", method = RequestMethod.GET)
    ResponseEntity<Iterable<Transaction>> getTransactions() {
        return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "getSingleTransaction/{transactionId}", method = RequestMethod.GET)
    ResponseEntity<Transaction> getSingleTransaction(@PathVariable String transactionId)
    {
        return new ResponseEntity<>(repository.getTransactionInfo(transactionId), HttpStatus.OK);
    }
    
    @RequestMapping(value = "filterTransaction", method = RequestMethod.GET)
    ResponseEntity<Iterable<Transaction>> getFilteredTransaction(@RequestParam String itemPIC, @RequestParam String itemEntrustedStatus) {
    	if (itemEntrustedStatus.equals("0") && itemPIC.equals("0"))
        {
    		return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
        }else if (!itemEntrustedStatus.equals("0") && !itemPIC.equals("0"))
        {
        	//entrustedstatus & PIC
        	return new ResponseEntity<>(repository.transactionFilteredAllQuery(itemPIC, itemEntrustedStatus), HttpStatus.OK);
        }else if (!itemEntrustedStatus.equals("0") && itemPIC.equals("0"))
        {
        	//entrustedstatus
        	return new ResponseEntity<>(repository.transactionFilteredWithoutPIC(itemEntrustedStatus), HttpStatus.OK);
        }else if (itemEntrustedStatus.equals("0") && !itemPIC.equals("0"))
        {
        	//PIC
        	return new ResponseEntity<>(repository.transactionFilteredWithoutEntrusted(itemPIC), HttpStatus.OK);
        }
        
		return null;
    }
    
    @RequestMapping(value="modifyTransaction", method = RequestMethod.POST)
    ResponseEntity<Transaction> modifyTransaction(@RequestParam(required = false) String transactionId, 
    										@RequestParam String itemPIC, @RequestParam String itemSoldDate,
    										@RequestParam String itemId,@RequestParam long itemSoldPrice)
    {
        Transaction target = new Transaction();

        if (transactionId!=null)
        {
            target = repository.findOne(itemId);
        }else
        {
            String dateId = itemSoldDate.replace("-", "");
            String generatedId = "TR" + dateId;
            long id = Long.parseLong(repository.maxTransactionId(generatedId).substring(10,13))+1;
            String resultId = generatedId + String.format("%03d", id);
            target.setTransactionId(resultId);
        }
        target.setItemId(itemId);
        target.setItemSoldPrice(itemSoldPrice);
        target.setTransactionPIC(itemPIC);
        
        try
        {
            Date transactionDate = new SimpleDateFormat("yyyy-MM-dd").parse(itemSoldDate);
            target.setTransactionDate(transactionDate);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            return new ResponseEntity<>(target, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        

        repository.save(target);

        return new ResponseEntity<>(target, HttpStatus.OK);
    }

    @RequestMapping(value = "modifyTransaction/{itemId}", method = RequestMethod.DELETE)
    ResponseEntity<Transaction> deleteSingleTransaction(@PathVariable String itemId) {

        Transaction target = repository.findOne(itemId);

        repository.deleteByItemId(itemId);
        return new ResponseEntity<>(target, HttpStatus.OK);
    }
    
    @RequestMapping(value = "getTransactionYear", method = RequestMethod.GET)
    ResponseEntity<Iterable<String>> getTransactionYear() {
    	int minYear = repository.getMinYearTransaction();
    	int nowYear = Calendar.getInstance().get(Calendar.YEAR);
    	List<String> resultList = new ArrayList<String>();
    	for (int i = nowYear;i>=minYear;i--)
    	{
    		resultList.add(String.valueOf(i));
    	}
        return new ResponseEntity<>(resultList, HttpStatus.OK);
    }
    
    @RequestMapping(value = "getTransactionReport", method = RequestMethod.GET)
    ResponseEntity<Iterable<Transaction>> getReport(
    										 @RequestParam int year, 
    										 @RequestParam int month,
    										 @RequestParam int entrustedStatus) {
		if (month > 0)
		{
			if (entrustedStatus == 0)
			{
				return new ResponseEntity<>(repository.reportTransactionMonthlyAll(year, month), HttpStatus.OK);
			}else if (entrustedStatus == 1)
			{
				return new ResponseEntity<>(repository.reportTransactionMonthlyEntrusted(year, month), HttpStatus.OK);
			}else if (entrustedStatus == 2)
			{
				return new ResponseEntity<>(repository.reportTransactionMonthlyNotEntrusted(year, month), HttpStatus.OK);
			}
		}else
		{
			if (entrustedStatus == 0)
			{
				return new ResponseEntity<>(repository.reportTransactionYearlyAll(year), HttpStatus.OK);
			}else if (entrustedStatus == 1)
			{
				return new ResponseEntity<>(repository.reportTransactionYearlyEntrusted(year), HttpStatus.OK);
			}else if (entrustedStatus == 2)
			{
				return new ResponseEntity<>(repository.reportTransactionYearlyNotEntrusted(year), HttpStatus.OK);
			}
		}
    	
		return null;
    }
}
