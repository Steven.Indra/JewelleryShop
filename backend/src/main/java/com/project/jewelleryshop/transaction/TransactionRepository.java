package com.project.jewelleryshop.transaction;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface TransactionRepository extends PagingAndSortingRepository<Transaction,String>
{
	@Query("select coalesce(max(it.transactionId),'TR00000000000') from Transaction it where it.transactionId like :generatedId%")
	public String maxTransactionId(@Param("generatedId") String generatedId);
	
	@Query("select it from Transaction it where it.transactionId = :transactionId")
	public Transaction getTransactionInfo(@Param("transactionId") String transactionId);
	
	@Transactional
    public void deleteByItemId (@Param("itemId") String itemId);
	
	@Query("select it from Transaction it where it.transactionPIC like %:itemPIC% and it.item.itemEntrustedStatus like %:itemEntrustedStatus%")
	public Iterable<Transaction> transactionFilteredAllQuery(@Param("itemPIC") String itemPIC, @Param("itemEntrustedStatus") String itemEntrustedStatus);
	
	@Query("select it from Transaction it where it.item.itemEntrustedStatus like %:itemEntrustedStatus%")
	public Iterable<Transaction> transactionFilteredWithoutPIC(@Param("itemEntrustedStatus") String itemEntrustedStatus);
	
	@Query("select it from Transaction it where it.transactionPIC like %:itemPIC%")
	public Iterable<Transaction> transactionFilteredWithoutEntrusted(@Param("itemPIC") String itemPIC);
	
	@Query("select MIN(YEAR(it.transactionDate)) from Transaction it")
	public int getMinYearTransaction();
	
	@Query("select it from Transaction it where YEAR(it.transactionDate) = :year")
	public Iterable<Transaction> reportTransactionYearlyAll(@Param("year") int year);
	
	@Query("select it from Transaction it where YEAR(it.transactionDate) = :year and it.item.itemEntrustedStatus is not null")
	public Iterable<Transaction> reportTransactionYearlyEntrusted(@Param("year") int year);
	
	@Query("select it from Transaction it where YEAR(it.transactionDate) = :year and it.item.itemEntrustedStatus = null")
	public Iterable<Transaction> reportTransactionYearlyNotEntrusted(@Param("year") int year);
	
	@Query("select it from Transaction it where YEAR(it.transactionDate) = :year and MONTH(it.transactionDate) = :month")
	public Iterable<Transaction> reportTransactionMonthlyAll(@Param("year") int year, @Param("month") int month);
	
	@Query("select it from Transaction it where YEAR(it.transactionDate) = :year and MONTH(it.transactionDate) = :month and it.item.itemEntrustedStatus is not null")
	public Iterable<Transaction> reportTransactionMonthlyEntrusted(@Param("year") int year, @Param("month") int month);
	
	@Query("select it from Transaction it where YEAR(it.transactionDate) = :year and MONTH(it.transactionDate) = :month and it.item.itemEntrustedStatus = null")
	public Iterable<Transaction> reportTransactionMonthlyNotEntrusted(@Param("year") int year, @Param("month") int month);
	
}
