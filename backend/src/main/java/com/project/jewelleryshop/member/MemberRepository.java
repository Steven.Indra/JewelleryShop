package com.project.jewelleryshop.member;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface MemberRepository extends PagingAndSortingRepository<Member,Long>
{
	@Transactional
    public void deleteByUserId (@Param("userId") long userId);
	
	@Query("select me from Member me where me.userId != 1")
	public Iterable<Member> getAllUserWithoutAdmin();
	
	@Query("select me from Member me where LOWER(me.userName) = LOWER(:username)")
	public Member tryLogin(@Param("username") String username);
}
