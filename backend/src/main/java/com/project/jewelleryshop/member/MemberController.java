package com.project.jewelleryshop.member;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.project.jewelleryshop.privilege.Privilege;
import com.project.jewelleryshop.privilege.PrivilegeRepository;

@RestController
public class MemberController {

    @Autowired
    private MemberRepository repository;
    
    @Autowired
    private PrivilegeRepository privilegeRepository;

    @RequestMapping(value = "getAllUser", method = RequestMethod.GET)
    ResponseEntity<Iterable<Member>> getAllUser() {
        return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "getAllUserNoAdmin", method = RequestMethod.GET)
    ResponseEntity<Iterable<Member>> getAllUserWithoutAdmin() {
        return new ResponseEntity<>(repository.getAllUserWithoutAdmin(), HttpStatus.OK);
    }

    @RequestMapping(value = "getSingleUser/{userId}", method = RequestMethod.GET)
    ResponseEntity<Member> getSingleUser(@PathVariable long userId)
    {
        return new ResponseEntity<>(repository.findOne(userId), HttpStatus.OK);
    }
    
    @RequestMapping(value = "tryLogin/{username}", method = RequestMethod.GET)
    ResponseEntity<Member> tryLogin(@PathVariable String username)
    {
        return new ResponseEntity<>(repository.tryLogin(username), HttpStatus.OK);
    }
    
    @RequestMapping(value="modifyUser", method = RequestMethod.POST)
    ResponseEntity<Member> modifyUser(@RequestParam(required = false) Long userId, 
    										@RequestParam String userName, 
    										@RequestParam String userPassword,
    										@RequestParam String userShownName,
    										@RequestParam(required = false) List<Long> userPrivileges)
    {
    	Member target;

        if (userId!=null)
            target = repository.findOne(userId);
        else
            target = new Member();
        
        target.setUserName(userName);
        target.setUserPassword(userPassword);
        target.setUserShownName(userShownName);        
        target.setPrivileges(buildPrivilege(userPrivileges));
        repository.save(target);

        return new ResponseEntity<>(target, HttpStatus.OK);
    }

    Set<Privilege> buildPrivilege(List<Long> userPrivileges)
    {
    	Boolean flagAdmin = false;
    	Set<Privilege> listPriv = new HashSet<Privilege>();
        for (int i=0;i< userPrivileges.size();i++)
        {
        	listPriv.add(privilegeRepository.findOne(userPrivileges.get(i)));
        }

        if (userPrivileges.contains(5L)) //contain CUD User
        {
        	flagAdmin = true;
        	listPriv.add(privilegeRepository.findOne(14L));//User Dashboard
        }
        
        if (userPrivileges.contains(6L) || userPrivileges.contains(7L) || userPrivileges.contains(8L) ) //contain add, edit, delete transaction
        {
        	flagAdmin = true;
        	listPriv.add(privilegeRepository.findOne(13L));//Transaction Dashboard
        }
        
        if (userPrivileges.contains(9L) || userPrivileges.contains(10L) || userPrivileges.contains(11L) ) //contain add, edit, delete category
        {
        	flagAdmin = true;
        	listPriv.add(privilegeRepository.findOne(12L));//Category Dashboard
        }
        
        if (userPrivileges.contains(16L)) //contain view report
        {
        	flagAdmin = true;
        	listPriv.add(privilegeRepository.findOne(15L));//Report Dashboard
        }
        
        if (userPrivileges.contains(18L)) //contain Bulk item
        {
        	flagAdmin = true;
        	listPriv.add(privilegeRepository.findOne(17L));//Bulk Dashboard
        }
        
        if (flagAdmin)
        {
        	listPriv.add(privilegeRepository.findOne(1L));//Admin Dashboard
        }
    	return listPriv;
    }
    
    @RequestMapping(value="changePassword", method = RequestMethod.POST)
    ResponseEntity<Member> changeUserPassword(@RequestParam Long userId, 
    										@RequestParam String userNewPassword)
    {
    	Member target;

        target = repository.findOne(userId);
        
        target.setUserPassword(userNewPassword);
        repository.save(target);

        return new ResponseEntity<>(target, HttpStatus.OK);
    }
    
    @RequestMapping(value = "modifyUser/{userId}", method = RequestMethod.DELETE)
    ResponseEntity<Member> deleteSingleCategory(@PathVariable long userId) {

    	Member target = repository.findOne(userId);

        repository.deleteByUserId(userId);
        return new ResponseEntity<>(target, HttpStatus.OK);
    }
}
