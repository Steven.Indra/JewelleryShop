package com.project.jewelleryshop.category;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface CategoryRepository extends PagingAndSortingRepository<Category,Long>
{
	@Transactional
    public void deleteByCategoryId (@Param("categoryId") long categoryId);
	
	@Query("select cat from Category cat where UPPER(cat.categoryName) = UPPER(:categoryName)")
	public Category getCategory(@Param("categoryName") String categoryName);
}
