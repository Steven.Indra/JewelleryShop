package com.project.jewelleryshop.category;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.project.jewelleryshop.item.Item;

import javax.persistence.*;
import java.util.List;

@Entity
public class Category {

    public Category(){}

    public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}	
	
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}



	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "category_id", nullable = false)
    private long categoryId;
    @Column(name = "category_name", nullable = false)
    private String categoryName;
    @Column(name = "description", nullable = true)
    private String description;
    
    @OneToMany(mappedBy = "itemCategory", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Item> items;
}
