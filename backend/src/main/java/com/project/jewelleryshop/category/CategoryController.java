package com.project.jewelleryshop.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CategoryController {

    @Autowired
    private CategoryRepository repository;

    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping(value = "getCategories", method = RequestMethod.GET)
    ResponseEntity<Iterable<Category>> getCategories() {
        return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "getSingleCategory/{categoryId}", method = RequestMethod.GET)
    ResponseEntity<Category> getSingleCategory(@PathVariable long categoryId)
    {
        return new ResponseEntity<>(repository.findOne(categoryId), HttpStatus.OK);
    }
    
    @RequestMapping(value="modifyCategory", method = RequestMethod.POST)
    ResponseEntity<Category> modifyCategory(@RequestParam(required = false) Long categoryId, 
    										@RequestParam String categoryName, 
    										@RequestParam(required = false) String description)
    {
        Category target;

        if (categoryId!=null)
            target = repository.findOne(categoryId);
        else
            target = new Category();
        
        target.setCategoryName(categoryName);
        target.setDescription(description);

        repository.save(target);

        return new ResponseEntity<>(target, HttpStatus.OK);
    }

    @RequestMapping(value = "modifyCategory/{categoryId}", method = RequestMethod.DELETE)
    ResponseEntity<Category> deleteSingleCategory(@PathVariable long categoryId) {

        Category target = repository.findOne(categoryId);

        repository.deleteByCategoryId(categoryId);
        return new ResponseEntity<>(target, HttpStatus.OK);
    }
}
