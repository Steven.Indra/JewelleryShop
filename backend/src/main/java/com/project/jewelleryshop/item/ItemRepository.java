package com.project.jewelleryshop.item;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface ItemRepository extends PagingAndSortingRepository<Item,String>
{
	@Transactional
    public void deleteByItemId (@Param("itemId") String itemId);
	
	@Query("select it from Item it where it.itemTransaction is empty")
	public Iterable<Item> itemNotSold();
	
	@Query("select it from Item it where it.itemTransaction is not empty and it.itemEntrustedStatus like %:itemEntrustedStatus% and it.itemCategory.categoryId = :categoryId")
	public Iterable<Item> itemFilteredAllQuerySold(@Param("categoryId") Long categoryId, @Param("itemEntrustedStatus") String itemEntrustedStatus);
	
	@Query("select it from Item it where it.itemTransaction is empty and it.itemEntrustedStatus like %:itemEntrustedStatus% and it.itemCategory.categoryId = :categoryId")
	public Iterable<Item> itemFilteredAllQueryNotSold(@Param("categoryId") Long categoryId, @Param("itemEntrustedStatus") String itemEntrustedStatus);	
	
	@Query("select it from Item it where it.itemEntrustedStatus like %:itemEntrustedStatus% and it.itemCategory.categoryId = :categoryId")
	public Iterable<Item> itemFilteredAllQueryNoneSold(@Param("categoryId") Long categoryId, @Param("itemEntrustedStatus") String itemEntrustedStatus);	
	
	@Query("select it from Item it where it.itemTransaction is not empty and it.itemEntrustedStatus like %:itemEntrustedStatus%")
	public Iterable<Item> itemFilteredWithoutCategorySold(@Param("itemEntrustedStatus") String itemEntrustedStatus);
	
	@Query("select it from Item it where it.itemTransaction is empty and it.itemEntrustedStatus like %:itemEntrustedStatus%")
	public Iterable<Item> itemFilteredWithoutCategoryNotSold(@Param("itemEntrustedStatus") String itemEntrustedStatus);
	
	@Query("select it from Item it where it.itemEntrustedStatus like %:itemEntrustedStatus%")
	public Iterable<Item> itemFilteredWithoutCategoryNoneSold(@Param("itemEntrustedStatus") String itemEntrustedStatus);
	
	@Query("select it from Item it where it.itemTransaction is not empty and it.itemCategory.categoryId = :categoryId")
	public Iterable<Item> itemFilteredWithoutEntrustedStatusSold(@Param("categoryId") Long categoryId);
	
	@Query("select it from Item it where it.itemTransaction is empty and it.itemCategory.categoryId = :categoryId")
	public Iterable<Item> itemFilteredWithoutEntrustedStatusNotSold(@Param("categoryId") Long categoryId);
	
	@Query("select it from Item it where it.itemCategory.categoryId = :categoryId")
	public Iterable<Item> itemFilteredWithoutEntrustedStatusNoneSold(@Param("categoryId") Long categoryId);
	
	@Query("select it from Item it where it.itemTransaction is not empty")
	public Iterable<Item> itemFilteredOnlySold();
	
	@Query("select MIN(YEAR(it.itemBuyDate)) from Item it where it.itemTransaction is empty")
	public int getMinYearStockItem();
	
	@Query("select MIN(YEAR(it.itemBuyDate)) from Item it")
	public int getMinYearIncomingItem();
	
	@Query("select MIN(YEAR(it.itemTransaction.transactionDate)) from Item it where it.itemTransaction is not empty")
	public int getMinYearExitItem();
	
	@Query("select it from Item it where it.itemTransaction is empty")
	public Iterable<Item> reportStockItemAll();
	
	@Query("select it from Item it where it.itemTransaction is empty and it.itemEntrustedStatus is not null")
	public Iterable<Item> reportStockItemEntrusted();
	
	@Query("select it from Item it where it.itemTransaction is empty and it.itemEntrustedStatus = null")
	public Iterable<Item> reportStockItemNotEntrusted();
	
	@Query("select it from Item it where YEAR(it.itemBuyDate) = :year")
	public Iterable<Item> reportIncomingItemYearlyAll(@Param("year") int year);
	
	@Query("select it from Item it where YEAR(it.itemBuyDate) = :year and it.itemEntrustedStatus is not null")
	public Iterable<Item> reportIncomingItemYearlyEntrusted(@Param("year") int year);
	
	@Query("select it from Item it where YEAR(it.itemBuyDate) = :year and it.itemEntrustedStatus = null")
	public Iterable<Item> reportIncomingItemYearlyNotEntrusted(@Param("year") int year);
	
	@Query("select it from Item it where YEAR(it.itemBuyDate) = :year and MONTH(it.itemBuyDate) = :month")
	public Iterable<Item> reportIncomingItemMonthlyAll(@Param("year") int year, @Param("month") int month);
	
	@Query("select it from Item it where YEAR(it.itemBuyDate) = :year and MONTH(it.itemBuyDate) = :month and it.itemEntrustedStatus is not null")
	public Iterable<Item> reportIncomingItemMonthlyEntrusted(@Param("year") int year, @Param("month") int month);
	
	@Query("select it from Item it where YEAR(it.itemBuyDate) = :year and MONTH(it.itemBuyDate) = :month and it.itemEntrustedStatus = null")
	public Iterable<Item> reportIncomingItemMonthlyNotEntrusted(@Param("year") int year, @Param("month") int month);
	
	@Query("select it from Item it where it.itemTransaction is not empty and YEAR(it.itemTransaction.transactionDate) = :year")
	public Iterable<Item> reportExitItemYearlyAll(@Param("year") int year);
	
	@Query("select it from Item it where it.itemTransaction is not empty and YEAR(it.itemTransaction.transactionDate) = :year and it.itemEntrustedStatus is not null")
	public Iterable<Item> reportExitItemYearlyEntrusted(@Param("year") int year);
	
	@Query("select it from Item it where it.itemTransaction is not empty and YEAR(it.itemTransaction.transactionDate) = :year and it.itemEntrustedStatus = null")
	public Iterable<Item> reportExitItemYearlyNotEntrusted(@Param("year") int year);
	
	@Query("select it from Item it where it.itemTransaction is not empty and YEAR(it.itemTransaction.transactionDate) = :year and MONTH(it.itemTransaction.transactionDate) = :month")
	public Iterable<Item> reportExitItemMonthlyAll(@Param("year") int year, @Param("month") int month);
	
	@Query("select it from Item it where it.itemTransaction is not empty and YEAR(it.itemTransaction.transactionDate) = :year and MONTH(it.itemTransaction.transactionDate) = :month and it.itemEntrustedStatus is not null")
	public Iterable<Item> reportExitItemMonthlyEntrusted(@Param("year") int year, @Param("month") int month);
	
	@Query("select it from Item it where it.itemTransaction is not empty and YEAR(it.itemTransaction.transactionDate) = :year and MONTH(it.itemTransaction.transactionDate) = :month and it.itemEntrustedStatus = null")
	public Iterable<Item> reportExitItemMonthlyNotEntrusted(@Param("year") int year, @Param("month") int month);
}
