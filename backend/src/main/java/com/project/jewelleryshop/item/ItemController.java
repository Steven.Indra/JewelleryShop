package com.project.jewelleryshop.item;

import com.project.jewelleryshop.category.Category;
import com.project.jewelleryshop.category.CategoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@RestController
public class ItemController {
	
	private String pathLocation = "./images/";
	
    @Autowired
    private ItemRepository itemRepository;
    
    @Autowired
    private CategoryRepository categoryRepository;
    
    private final String notSold = "Not Sold";
    private final String sold = "Sold";
    private final String none = "None";

    @RequestMapping(value = "getItems", method = RequestMethod.GET)
    ResponseEntity<Iterable<Item>> getItems() {    	
        return new ResponseEntity<>(itemRepository.findAll(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "getItemsNotSold", method = RequestMethod.GET)
    ResponseEntity<Iterable<Item>> getItemsNotSold() {
        return new ResponseEntity<>(itemRepository.itemNotSold(), HttpStatus.OK);
    }

    @RequestMapping(value = "getSingleItem/{itemId}", method = RequestMethod.GET)
    ResponseEntity<Item> getSingleItem(@PathVariable String itemId) {
        if (!itemRepository.exists(itemId))
            return new ResponseEntity<>(new Item(), HttpStatus.NOT_FOUND);
        else
        	System.out.println(itemRepository.findOne(itemId));
            return new ResponseEntity<>(itemRepository.findOne(itemId), HttpStatus.OK);
    }
    
    @RequestMapping(value = "getItemYear", method = RequestMethod.GET)
    ResponseEntity<Iterable<String>> getItemYear(@RequestParam String reportType) {
    	int minYear = 0;
    	if (reportType.equals("Incoming Item"))
    	{
    		minYear = itemRepository.getMinYearIncomingItem();
    	}else if (reportType.equals("Exit Item"))
    	{
    		minYear = itemRepository.getMinYearExitItem();
    	}
    	int nowYear = Calendar.getInstance().get(Calendar.YEAR);
    	List<String> resultList = new ArrayList<String>();
    	for (int i = nowYear;i>=minYear;i--)
    	{
    		resultList.add(String.valueOf(i));
    	}
        return new ResponseEntity<>(resultList, HttpStatus.OK);
    }
    
    @RequestMapping(value = "getReport", method = RequestMethod.GET)
    ResponseEntity<Iterable<Item>> getReport(@RequestParam String reportType, 
    										 @RequestParam int year, 
    										 @RequestParam int month,
    										 @RequestParam int entrustedStatus) {
    	if (reportType.equals("Stock Item"))
    	{
    		if (entrustedStatus == 0)
			{
				return new ResponseEntity<>(itemRepository.reportStockItemAll(), HttpStatus.OK);
			}else if (entrustedStatus == 1)
			{
				return new ResponseEntity<>(itemRepository.reportStockItemEntrusted(), HttpStatus.OK);
			}else if (entrustedStatus == 2)
			{
				return new ResponseEntity<>(itemRepository.reportStockItemNotEntrusted(), HttpStatus.OK);
			}
    	}else if (reportType.equals("Incoming Item"))
    	{
    		if (month > 0)
    		{
    			if (entrustedStatus == 0)
    			{
    				return new ResponseEntity<>(itemRepository.reportIncomingItemMonthlyAll(year, month), HttpStatus.OK);
    			}else if (entrustedStatus == 1)
    			{
    				return new ResponseEntity<>(itemRepository.reportIncomingItemMonthlyEntrusted(year, month), HttpStatus.OK);
    			}else if (entrustedStatus == 2)
    			{
    				return new ResponseEntity<>(itemRepository.reportIncomingItemMonthlyNotEntrusted(year, month), HttpStatus.OK);
    			}
    		}else
    		{
    			if (entrustedStatus == 0)
    			{
    				return new ResponseEntity<>(itemRepository.reportIncomingItemYearlyAll(year), HttpStatus.OK);
    			}else if (entrustedStatus == 1)
    			{
    				return new ResponseEntity<>(itemRepository.reportIncomingItemYearlyEntrusted(year), HttpStatus.OK);
    			}else if (entrustedStatus == 2)
    			{
    				return new ResponseEntity<>(itemRepository.reportIncomingItemYearlyNotEntrusted(year), HttpStatus.OK);
    			}
    		}
    	}else if (reportType.equals("Exit Item"))
    	{
    		if (month > 0)
    		{
    			if (entrustedStatus == 0)
    			{
    				return new ResponseEntity<>(itemRepository.reportExitItemMonthlyAll(year, month), HttpStatus.OK);
    			}else if (entrustedStatus == 1)
    			{
    				return new ResponseEntity<>(itemRepository.reportExitItemMonthlyEntrusted(year, month), HttpStatus.OK);
    			}else if (entrustedStatus == 2)
    			{
    				return new ResponseEntity<>(itemRepository.reportExitItemMonthlyNotEntrusted(year, month), HttpStatus.OK);
    			}
    		}else
    		{
    			if (entrustedStatus == 0)
    			{
    				return new ResponseEntity<>(itemRepository.reportExitItemYearlyAll(year), HttpStatus.OK);
    			}else if (entrustedStatus == 1)
    			{
    				return new ResponseEntity<>(itemRepository.reportExitItemYearlyEntrusted(year), HttpStatus.OK);
    			}else if (entrustedStatus == 2)
    			{
    				return new ResponseEntity<>(itemRepository.reportExitItemYearlyNotEntrusted(year), HttpStatus.OK);
    			}
    		}
    	}
        
		return null;
    }
    
    @RequestMapping(value = "filterItem", method = RequestMethod.GET)
    ResponseEntity<Iterable<Item>> getFilteredItems(@RequestParam Long itemCategoryId, @RequestParam String itemEntrustedStatus, @RequestParam Long itemSold) {
    	String soldValue = sold;
        if (itemSold == 0)
        {
        	soldValue = notSold;
        }else if (itemSold == -1)
        {
        	soldValue = none;
        }
    	if (itemEntrustedStatus.equals("0") && itemCategoryId == -1)
        {
    		//no category & entrustedstatus
        	if (soldValue.equals(sold))
        	{
        		//item sold
        		return new ResponseEntity<>(itemRepository.itemFilteredOnlySold(), HttpStatus.OK);
        	}else if (soldValue.equals(notSold))
        	{
        		//item not sold
        		return new ResponseEntity<>(itemRepository.itemNotSold(), HttpStatus.OK);
        	}else
        	{
        		//no filter -> all
        		return new ResponseEntity<>(itemRepository.findAll(), HttpStatus.OK);
        	}
        }else if (!itemEntrustedStatus.equals("0") && itemCategoryId != -1)
        {
        	//category & entrustedstatus
        	if (soldValue.equals(sold))
        	{
        		//item sold
        		return new ResponseEntity<>(itemRepository.itemFilteredAllQuerySold(itemCategoryId, itemEntrustedStatus), HttpStatus.OK);
        	}else if (soldValue.equals(notSold))
        	{
        		//item not sold
        		return new ResponseEntity<>(itemRepository.itemFilteredAllQueryNotSold(itemCategoryId, itemEntrustedStatus), HttpStatus.OK);
        	}else
        	{
        		//none sold query
        		return new ResponseEntity<>(itemRepository.itemFilteredAllQueryNoneSold(itemCategoryId, itemEntrustedStatus), HttpStatus.OK);
        	}
        }else if (!itemEntrustedStatus.equals("0") && itemCategoryId == -1)
        {
        	//without category
        	if (soldValue.equals(sold))
        	{
        		//item sold
        		return new ResponseEntity<>(itemRepository.itemFilteredWithoutCategorySold(itemEntrustedStatus), HttpStatus.OK);
        	}else if (soldValue.equals(notSold))
        	{
        		//item not sold
        		return new ResponseEntity<>(itemRepository.itemFilteredWithoutCategoryNotSold(itemEntrustedStatus), HttpStatus.OK);
        	}else
        	{
        		//none sold query
        		return new ResponseEntity<>(itemRepository.itemFilteredWithoutCategoryNoneSold(itemEntrustedStatus), HttpStatus.OK);
        	}
        }else if (itemEntrustedStatus.equals("0") && itemCategoryId != -1)
        {
        	//without entrustedStatus
        	if (soldValue.equals(sold))
        	{
        		//item sold
        		return new ResponseEntity<>(itemRepository.itemFilteredWithoutEntrustedStatusSold(itemCategoryId), HttpStatus.OK);
        	}else if (soldValue.equals(notSold))
        	{
        		//item not sold
        		return new ResponseEntity<>(itemRepository.itemFilteredWithoutEntrustedStatusNotSold(itemCategoryId), HttpStatus.OK);
        	}else
        	{
        		//none sold query
        		return new ResponseEntity<>(itemRepository.itemFilteredWithoutEntrustedStatusNoneSold(itemCategoryId), HttpStatus.OK);
        	}
        }
        
		return null;
    }
    @RequestMapping(value="importBulkItem", method = RequestMethod.POST)
    ResponseEntity<Item> importBulkItem(@RequestParam String itemId, @RequestParam String itemName,
                                       @RequestParam double itemWeight, @RequestParam double itemGoldContent, @RequestParam long itemBuyPrice,
                                       @RequestParam String itemBuyDate, @RequestParam String itemCategoryName,
                                       @RequestParam(required = false) String itemEntrustedStatus, @RequestParam(required = false) String itemNote )
    {
    	Category targetCategory = categoryRepository.getCategory(itemCategoryName);


    	
        Item target = new Item();
        
        if (targetCategory == null)
    	{
    		return new ResponseEntity<>(target, HttpStatus.NOT_ACCEPTABLE);
    	}
        
    	try
    	{
    		target = itemRepository.findOne(itemId);
    		if (target.getItemName() != null)
    		{
//        			System.out.println("unique");
    			return new ResponseEntity<>(target, HttpStatus.CONFLICT);
    		}
    	}catch(Exception ex)
    	{
    		target = new Item();
    		target.setItemId(itemId);
    	}

        target.setItemName(itemName);

        try
        {
            Date buyDate = new SimpleDateFormat("dd/MM/yyyy").parse(itemBuyDate);
            target.setItemBuyDate(buyDate);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            return new ResponseEntity<>(target, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        target.setItemWeight(itemWeight);
        target.setItemGoldContent(itemGoldContent);
        target.setItemBuyPrice(itemBuyPrice);
        target.setItemCategory(targetCategory);
        if (!itemEntrustedStatus.isEmpty())
        {
        	if (itemEntrustedStatus.equals(""))
            {
            	itemEntrustedStatus = null;
            }
        }
        
        target.setItemEntrustedStatus(itemEntrustedStatus);
        target.setItemNote(itemNote);
        itemRepository.save(target);
    	return new ResponseEntity<>(target, HttpStatus.OK);
    }
    
    @RequestMapping(value="modifyItem", method = RequestMethod.POST)
    ResponseEntity<Item> modifyItem(@RequestParam String itemId, @RequestParam String itemName,
                                       @RequestParam double itemWeight, @RequestParam(required = false) MultipartFile itemPhoto,
                                       @RequestParam double itemGoldContent, @RequestParam long itemBuyPriceFormatted,
                                       @RequestParam String itemBuyDate, @RequestParam long itemCategoryId,
                                       @RequestParam(required = false) String itemEntrustedStatusChanged, @RequestParam(required = false) String itemNote,
                                       @RequestParam long isAdd )
    {
        Category targetCategory = categoryRepository.findOne(itemCategoryId);
        Item target = new Item();

        if (isAdd==0)
        {
            target = itemRepository.findOne(itemId);
        }
        else
        {
        	try
        	{
        		target = itemRepository.findOne(itemId);
        		if (target.getItemName() != null)
        		{
//        			System.out.println("unique");
        			return new ResponseEntity<>(target, HttpStatus.CONFLICT);
        		}
        	}catch(Exception ex)
        	{
        		target = new Item();
        		target.setItemId(itemId);
        	}
        }

        if (!itemPhoto.isEmpty())
        {
            //String[] matches = {"jpeg", "png"};
            String fileMimeType = itemPhoto.getContentType();

            if ((!fileMimeType.contains("jpeg") && !fileMimeType.contains("png")) || itemPhoto.getSize() > 1048576)
                return new ResponseEntity<>(target, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
            else
            {
//                String extension;
//                if (fileMimeType.contains("jpeg"))
//                    extension = ".jpg";
//                else
//                    extension = ".png";

                String filename = itemId.concat("-"+itemName+"-").concat(UUID.randomUUID().toString());
                //String filename = firstName.concat("-"+lastName).concat(extension);
                //File targetFile = new File("./../frontend/src/assets/" + filename);
                File targetFile = new File(pathLocation + filename + ".jpg");

                try
                {
                    targetFile.createNewFile();
                    itemPhoto.transferTo(targetFile.getAbsoluteFile());
                	
                    //Delete Old Photo
                    if (target.getItemPhoto() != null)
                        new File("./images/" + target.getItemPhoto() + ".jpg").getAbsoluteFile().delete();
                    
                    target.setItemPhoto(filename);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    return new ResponseEntity<>(target, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        }

        target.setItemName(itemName);

        try
        {
            Date buyDate = new SimpleDateFormat("yyyy-MM-dd").parse(itemBuyDate);
            target.setItemBuyDate(buyDate);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            return new ResponseEntity<>(target, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        target.setItemWeight(itemWeight);
        target.setItemGoldContent(itemGoldContent);
        target.setItemBuyPrice(itemBuyPriceFormatted);
        target.setItemCategory(targetCategory);
        if (!itemEntrustedStatusChanged.isEmpty())
        {
        	if (itemEntrustedStatusChanged.equals(""))
            {
        		itemEntrustedStatusChanged = null;
            }
        }
        
        target.setItemEntrustedStatus(itemEntrustedStatusChanged);
        target.setItemNote(itemNote);
        itemRepository.save(target);

        return new ResponseEntity<>(target, HttpStatus.OK);
    }

    @RequestMapping(value = "modifyItem/{itemId}", method = RequestMethod.DELETE)
    ResponseEntity<Item> deleteSingleItem(@PathVariable String itemId) {

        Item target = itemRepository.findOne(itemId);

        if (target.getItemPhoto() != null)
            new File("./images/" + target.getItemPhoto() + ".jpg").getAbsoluteFile().delete();

        itemRepository.deleteByItemId(itemId);
        return new ResponseEntity<>(target, HttpStatus.OK);
    }
    
    public Resource loadFile(String filename) {
		try {
			Path file = Paths.get(pathLocation).resolve(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("FAIL!");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("FAIL!");
		}
	}
    
    @RequestMapping(value = "/getImage/{filename}", method = RequestMethod.GET,
            produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getImage(@PathVariable String filename) throws IOException {
        
    	Path file = Paths.get("./images/").resolve(filename + ".jpg");
    	Resource resource = new UrlResource(file.toUri());
    	InputStream input = resource.getInputStream();
        byte[] bytes = StreamUtils.copyToByteArray(input);
        input.close();
        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(bytes);
    }
}
