package com.project.jewelleryshop.item;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.project.jewelleryshop.category.Category;
import com.project.jewelleryshop.transaction.Transaction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="item")
@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@id")
public class Item
{
    public Item() {}

    @Id
    @Column(name="item_id", nullable = false)
    private String itemId;
    @Column(name="item_name", nullable = false)
    private String itemName;
    @Column(name="item_weight", nullable = false)
    private double itemWeight;
    @Column(name="item_gold_content", nullable = false)
    private double itemGoldContent;
    @Column(name="item_buy_price", nullable = false)
    private long itemBuyPrice;
    @Column(name="item_buy_date", nullable = false)
    private Date itemBuyDate;
    @ManyToOne()
    @JoinColumn(name = "item_category_id", nullable = false)
    private Category itemCategory;
    @Column(name="item_photo")
    private String itemPhoto;
    @Column(name="item_entrusted_by")
    private String itemEntrustedStatus;
    @Column(name="item_note")
    private String itemNote;
    @OneToOne(mappedBy = "item")
    private Transaction itemTransaction;
    
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getItemWeight() {
		return itemWeight;
	}
	public void setItemWeight(double itemWeight) {
		this.itemWeight = itemWeight;
	}
	public double getItemGoldContent() {
		return itemGoldContent;
	}
	public void setItemGoldContent(double itemGoldContent) {
		this.itemGoldContent = itemGoldContent;
	}
	public long getItemBuyPrice() {
		return itemBuyPrice;
	}
	public void setItemBuyPrice(long itemBuyPrice) {
		this.itemBuyPrice = itemBuyPrice;
	}
	public Date getItemBuyDate() {
		return itemBuyDate;
	}
	public void setItemBuyDate(Date itemBuyDate) {
		this.itemBuyDate = itemBuyDate;
	}
	public Category getItemCategory() {
		return itemCategory;
	}
	public void setItemCategory(Category itemCategory) {
		this.itemCategory = itemCategory;
	}
	public String getItemPhoto() {
		return itemPhoto;
	}
	public void setItemPhoto(String itemPhoto) {
		this.itemPhoto = itemPhoto;
	}
	public Transaction getItemTransaction() {
		return itemTransaction;
	}
	public void setItemTransaction(Transaction itemTransaction) {
		this.itemTransaction = itemTransaction;
	}
	public String getItemEntrustedStatus() {
		return itemEntrustedStatus;
	}
	public void setItemEntrustedStatus(String itemEntrustedStatus) {
		this.itemEntrustedStatus = itemEntrustedStatus;
	}
	public String getItemNote() {
		return itemNote;
	}
	public void setItemNote(String itemNote) {
		this.itemNote = itemNote;
	}
	
}


