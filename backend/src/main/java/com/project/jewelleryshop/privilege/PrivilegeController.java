package com.project.jewelleryshop.privilege;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PrivilegeController {

    @Autowired
    private PrivilegeRepository repository;

    @RequestMapping(value = "getAllPrivilege", method = RequestMethod.GET)
    ResponseEntity<Iterable<Privilege>> getAllPrivilege() {
        return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "getAllNewUserPrivilege", method = RequestMethod.GET)
    ResponseEntity<Iterable<Privilege>> getAllNewUserPrivilege() {
        return new ResponseEntity<>(repository.getAllNewUserPrivilege(), HttpStatus.OK);
    }

    @RequestMapping(value = "getSinglePrivilege/{privilegeId}", method = RequestMethod.GET)
    ResponseEntity<Privilege> getSinglePrivilege(@PathVariable long privilegeId)
    {
        return new ResponseEntity<>(repository.findOne(privilegeId), HttpStatus.OK);
    }
}
