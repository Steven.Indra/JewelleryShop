package com.project.jewelleryshop.privilege;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.project.jewelleryshop.member.Member;


@Entity
@Table(name="user_privilege")
public class Privilege {
	public Privilege() {}
	
	public long getPrivilegeId() {
		return privilegeId;
	}
	public void setPrivilegeId(long privilegeId) {
		this.privilegeId = privilegeId;
	}
	public String getPrivilegeName() {
		return privilegeName;
	}
	public void setPrivilegeName(String privilegeName) {
		this.privilegeName = privilegeName;
	}
	public String getPrivilegeDescription() {
		return privilegeDescription;
	}
	public void setPrivilegeDescription(String privilegeDescription) {
		this.privilegeDescription = privilegeDescription;
	}

	public Set<Member> getUsers() {
		return users;
	}

	public void setUsers(Set<Member> users) {
		this.users = users;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "privilege_id", nullable = false)
    private long privilegeId;
    @Column(name = "privilege_name", nullable = false)
    private String privilegeName;
    @Column(name = "privilege_description", nullable = false)
    private String privilegeDescription;
    @ManyToMany(mappedBy = "privileges")
    @JsonIgnore
    private Set<Member> users = new HashSet<>();
}
