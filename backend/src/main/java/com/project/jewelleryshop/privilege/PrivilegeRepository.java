package com.project.jewelleryshop.privilege;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PrivilegeRepository extends PagingAndSortingRepository<Privilege,Long>
{
	
	@Query("select pr from Privilege pr where pr.privilegeId != 5 and pr.privilegeName not like '%Dashboard%'")
	public Iterable<Privilege> getAllNewUserPrivilege();
}
