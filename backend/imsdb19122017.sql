-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2017 at 09:59 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `description` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `description`) VALUES
(1, 'Anting', 'weqweqweqweqweqweqweqweqwhegqwegqhgeqhgwehgqwhegqhwgehqghegqhwehqwgehqgwhegqhwwqeqweqqqq'),
(2, 'Kalung', 'asd'),
(4, 'Giwang', ''),
(5, 'Cincin', 'qweq'),
(10, 'qweqw', 'zz'),
(11, 'zzzz', 'qsdqw');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `item_id` varchar(10) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `item_weight` decimal(10,4) NOT NULL,
  `item_gold_content` decimal(10,2) NOT NULL,
  `item_buy_price` int(20) NOT NULL,
  `item_buy_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `item_photo` varchar(256) DEFAULT NULL,
  `item_category_id` int(11) NOT NULL,
  `item_entrusted_by` varchar(255) DEFAULT NULL,
  `item_note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`item_id`, `item_name`, `item_weight`, `item_gold_content`, `item_buy_price`, `item_buy_date`, `item_photo`, `item_category_id`, `item_entrusted_by`, `item_note`) VALUES
('C0001', 'testa', '1.0000', '1.23', 1233333, '2017-12-15 00:00:00', NULL, 5, 'tel', ''),
('C0002', 'qwe', '12.0000', '1.00', 123, '2017-12-15 00:00:00', NULL, 4, NULL, 'qwe'),
('C0003', 'asdas', '123.0000', '1231.00', 1, '2017-12-14 00:00:00', NULL, 5, '', 'qwe'),
('C0010', 'testa', '1.0000', '1.23', 100, '2018-01-15 00:00:00', NULL, 5, 'telmi', ''),
('C0011', 'testa2', '1.0000', '1.23', 100, '2018-01-15 00:00:00', NULL, 5, 'telmi2', ''),
('C0012', 'testa2', '1.0000', '1.23', 100, '2018-01-15 00:00:00', NULL, 5, 'telmi2', ''),
('C0013', 'testa2', '1.0000', '1.23', 100, '2018-01-15 00:00:00', NULL, 5, 'telmi2', ''),
('C0014', 'testa2', '1.0000', '1.23', 100, '2018-01-15 00:00:00', NULL, 5, 'telmi2', ''),
('K0001', 'qwe', '1.1000', '1.20', 1231231, '2017-11-30 00:00:00', NULL, 2, NULL, ''),
('K0002', 'new', '1.0000', '1.00', 1, '2017-12-20 00:00:00', NULL, 2, '', 'q'),
('qweq', 'qwe', '12131.1231', '1.12', 1232131231, '2015-11-17 00:00:00', 'qweq-qwe-42378944-e609-4d55-aecf-bae3d260cc1e', 2, NULL, NULL),
('QWEQW', 'qew', '123.1231', '12.12', 1231231200, '2017-11-18 00:00:00', NULL, 2, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `item_transaction`
--

CREATE TABLE `item_transaction` (
  `transaction_id` varchar(13) NOT NULL,
  `transaction_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `item_id` varchar(10) NOT NULL,
  `item_sold_price` int(20) NOT NULL,
  `person_in_charge` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_transaction`
--

INSERT INTO `item_transaction` (`transaction_id`, `transaction_date`, `item_id`, `item_sold_price`, `person_in_charge`) VALUES
('TR20171204001', '2017-12-04 00:00:00', 'QWEQW', 200000, 'susi');

-- --------------------------------------------------------

--
-- Table structure for table `member_privilege`
--

CREATE TABLE `member_privilege` (
  `user_id` int(11) NOT NULL,
  `privilege_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_privilege`
--

INSERT INTO `member_privilege` (`user_id`, `privilege_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(2, 1),
(2, 2),
(2, 6),
(2, 13),
(3, 1),
(3, 3),
(3, 4),
(3, 6),
(3, 9),
(3, 10),
(3, 12),
(3, 13);

-- --------------------------------------------------------

--
-- Table structure for table `user_member`
--

CREATE TABLE `user_member` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `user_password` varchar(20) NOT NULL,
  `user_shown_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_member`
--

INSERT INTO `user_member` (`user_id`, `user_name`, `user_password`, `user_shown_name`) VALUES
(1, 'admin', 'admin', 'Administrator'),
(2, 'User1', 'test1', 'Test User 1'),
(3, 'user2', 'user2', 'Test User 2');

-- --------------------------------------------------------

--
-- Table structure for table `user_privilege`
--

CREATE TABLE `user_privilege` (
  `privilege_id` int(11) NOT NULL,
  `privilege_name` varchar(50) NOT NULL,
  `privilege_description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_privilege`
--

INSERT INTO `user_privilege` (`privilege_id`, `privilege_name`, `privilege_description`) VALUES
(1, 'Admin Dashboard', 'Privilege for access admin dashboard menu'),
(2, 'Add Item', 'Privilege for add item'),
(3, 'Edit Item', 'Privilege for edit item'),
(4, 'Delete Item', 'Privilege for delete item'),
(5, 'CUD User', 'Privilege for create, edit, and delete user'),
(6, 'Add Transaction', 'Privilege for add transaction'),
(7, 'Edit Transaction', 'Privilege for edit transaction'),
(8, 'Delete Transaction', 'Privilege for delete transaction'),
(9, 'Add Category', 'Privilege for add category'),
(10, 'Edit Category', 'Privilege for edit category'),
(11, 'Delete Category', 'Privilege for delete category'),
(12, 'Category Dashboard', 'Privilege for access category dashboard'),
(13, 'Transaction Dashboard', 'Privilege for access transaction dashboard'),
(14, 'User Dashboard', 'Privilege for access user dashboard'),
(15, 'Report Dashboard', 'Privilege for access report dashboard menu'),
(16, 'View Report', 'Privilege for viewing report'),
(17, 'Bulk Dashboard', 'Privilege for access bulk dashboard menu'),
(18, 'Bulk Item', 'Privilege for using bulk item');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `FKcftuoeugjk7u1156bgace3p4p` (`item_category_id`);

--
-- Indexes for table `item_transaction`
--
ALTER TABLE `item_transaction`
  ADD PRIMARY KEY (`item_id`),
  ADD UNIQUE KEY `transaction_id` (`transaction_id`),
  ADD KEY `FKdki130fwuhd67c8c4av8tjtgi` (`item_id`);

--
-- Indexes for table `member_privilege`
--
ALTER TABLE `member_privilege`
  ADD PRIMARY KEY (`user_id`,`privilege_id`),
  ADD KEY `FK5ovfx6bp9fwrglnmlgc4ihf1t` (`privilege_id`);

--
-- Indexes for table `user_member`
--
ALTER TABLE `user_member`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`);

--
-- Indexes for table `user_privilege`
--
ALTER TABLE `user_privilege`
  ADD PRIMARY KEY (`privilege_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `user_member`
--
ALTER TABLE `user_member`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_privilege`
--
ALTER TABLE `user_privilege`
  MODIFY `privilege_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `FKcftuoeugjk7u1156bgace3p4p` FOREIGN KEY (`item_category_id`) REFERENCES `category` (`category_id`),
  ADD CONSTRAINT `fk_category_id` FOREIGN KEY (`item_category_id`) REFERENCES `category` (`category_id`);

--
-- Constraints for table `item_transaction`
--
ALTER TABLE `item_transaction`
  ADD CONSTRAINT `FKdki130fwuhd67c8c4av8tjtgi` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`);

--
-- Constraints for table `member_privilege`
--
ALTER TABLE `member_privilege`
  ADD CONSTRAINT `FK5ovfx6bp9fwrglnmlgc4ihf1t` FOREIGN KEY (`privilege_id`) REFERENCES `user_privilege` (`privilege_id`),
  ADD CONSTRAINT `FKnfejlekdi91ild0ev0sfpu1mj` FOREIGN KEY (`user_id`) REFERENCES `user_member` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
