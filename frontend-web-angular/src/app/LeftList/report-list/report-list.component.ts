import { Component, Inject, Input, OnInit } from '@angular/core';
import { CategoryService } from './../../service/category.service';
import { SharedService } from './../../service/shared-service.service';
import { Router } from '@angular/router';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { UtilityToken } from './../../providers';
import { Subscription } from 'rxjs/Subscription';
import { DeleteDialogComponent } from './../../dialog/delete-dialog/delete-dialog.component';
import { SessionService } from 'app/service/session.service';
import { ErrorDialogComponent } from './../../dialog/error-dialog/error-dialog.component';

@Component({
  selector: 'app-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.css']
})
export class ReportListComponent {

    selectedReport;
    //reportList = ["Stock Item","Incoming Item","Exit Item","Transaction"];
    reportList = ["Stok Barang","Barang Masuk","Barang Keluar", "Transaksi"];
    userLoggedData;
    private subscription: Subscription;
    constructor(
        private router: Router,
        private sessionService: SessionService,
        private sharedService: SharedService,
        public dialog: MatDialog,
    ) 
    {}

    ngOnInit() {
        this.checkPath();
        this.subscription = this.sharedService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('option') && res.option === 'highlight')
            {
                if (!this.selectedReport)
                {
                    this.selectedReport = res.value;
                }
            }
            else if (res.hasOwnProperty('option') && res.option === 'unhighlight')
            {
                this.resetSelection();
            }
        });
    }

    checkPath()
    {
        if (this.router.url.indexOf('report/stockitem') >= 0)
        {
            this.checkSelectedReport("Stock Item");
        }else if (this.router.url.indexOf('report/incomingitem') >= 0)
        {
            this.checkSelectedReport("Incoming Item");
        }else if (this.router.url.indexOf('report/exititem') >= 0)
        {
            this.checkSelectedReport("Exit Item");
        }else if (this.router.url.indexOf('report/transaction') >= 0)
        {
            this.checkSelectedReport("Transaction");
        }
    }

    checkSelectedReport(reportItem)
    {
        this.selectedReport = reportItem;
    }

    resetSelection(routeHome:boolean = true)
    {
        this.selectedReport = undefined;

        if (routeHome)
            this.router.navigate(['/admin/report/dashboard']);
    }

    onReportItemClick(reportItem)
    {
        this.selectedReport = reportItem;
        if (reportItem == "Stok Barang")
        {
            this.router.navigate(['admin/report/stockitem']);
        }else if (reportItem == "Barang Masuk")
        {
            this.router.navigate(['admin/report/incomingitem']);
        }else if (reportItem == "Barang Keluar")
        {
            this.router.navigate(['admin/report/exititem']);
        }else if (reportItem == "Transaksi")
        {
            this.router.navigate(['admin/report/transaction']);
        }
    }

    backToAdminDashboard()
    {
        this.router.navigate(['admin/dashboard']);
    }

    openErrorDialog(errorTitle, errorMessage)
    {
      let config = new MatDialogConfig();
      let dialogRef = this.dialog.open(ErrorDialogComponent, config);
  
      dialogRef.componentInstance.errorTitle = errorTitle;
      dialogRef.componentInstance.errorMessage = errorMessage;
    }
}