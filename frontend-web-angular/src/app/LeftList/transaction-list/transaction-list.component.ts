import { Component, Inject, Input, OnInit } from '@angular/core';
import { TransactionService } from './../../service/transaction.service';
import { SharedService } from './../../service/shared-service.service';
import { Router } from '@angular/router';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { UtilityToken } from './../../providers';
import { Subscription } from 'rxjs/Subscription';
import { DeleteDialogComponent } from './../../dialog/delete-dialog/delete-dialog.component';
import { TransactionFilterDialogComponent } from './../../dialog/transaction-filter-dialog/transaction-filter-dialog.component';
import { SessionService } from 'app/service/session.service';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.css']
})
export class TransactionListComponent {

    selectedTransaction;
    deleteTarget;
    transactionList = [];
    myTransactions;
    transactionOriginalList;
    sortCode = 2;
    activeFilter = {
        itemPIC             : false,
        itemEntrustedStatus : false
    };
    privileges = {
        addTransaction: false,
        deleteTransaction: false
    };
    private subscription: Subscription;

    constructor(
        private transactionService: TransactionService,
        private router: Router,
        private sharedService: SharedService,
        public snackBar: MatSnackBar,
        public dialog: MatDialog,
        private sessionService: SessionService
    ) 
    {}

    ngOnInit() {

        this.getTransactions();

        this.checkPrivilege();
        this.subscription = this.sharedService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('option') && res.option === 'refresh') 
            {
                //this.initiateFilter(false, false);
                this.getTransactions();
            }
            else if (res.hasOwnProperty('option') && res.option === 'highlight')
            {
                if (!this.selectedTransaction)
                {
                this.selectedTransaction = res.value;
                this.deleteTarget = this.selectedTransaction.transactionId;
                }
            }
            else if (res.hasOwnProperty('option') && res.option === 'unhighlight')
            {
                this.resetSelection();
            }else if (res.hasOwnProperty('option') && res.option === 'loginStatus' && res.value === "login") 
            {
                this.checkPrivilege();
            }else if (res.hasOwnProperty('option') && res.option === 'loginStatus' && res.value === "logout") 
            {
                this.checkPrivilege();
                this.selectedTransaction = null;
            }
        });
    }

    checkPrivilege()
    {
        this.privileges.addTransaction = this.sessionService.checkPrivilege("Add Transaction");
        this.privileges.deleteTransaction = this.sessionService.checkPrivilege("Delete Transaction");
    }

    backToAdminDashboard()
    {
        this.router.navigate(['admin/dashboard']);
    }

    sortItems()
    {
        if (this.sortCode === 0)
        {
            this.myTransactions.sort(function(a, b)
            {
                return a.transactionId - b.transactionId;
            });
        }
        else if (this.sortCode === 1)
        {
            this.myTransactions.sort(function(a, b)
            {
                return a.transactionId.localeCompare(b.transactionId);
            });
        }
        else if (this.sortCode === 2)
        {
            this.myTransactions.sort(function(a, b)
            {
                return b.transactionId.localeCompare(a.transactionId);
            });
        }
    }

    initiateSort()
    {
        let sortMeaning;

        this.sortCode++;
        if (this.sortCode > 2)
        this.sortCode = 1;

        switch (this.sortCode)
        {
        case 0:
            sortMeaning = 'Default';
            break;
        case 1:
            sortMeaning = 'terkecil ke terbesar';
            break;
        case 2:
            sortMeaning = 'terbesar ke terkecil';
            break;
        default:
            break;
        }

        this.resetSelection();
        this.sortItems();

        this.snackBar.open(`Id Transaksi diurutkan dari ${sortMeaning}`, 'OK',
        {
        duration: 1500,
        });
    }


    initiateSearch($event)
    {
        const query = $event.target.value.toLowerCase();
        this.resetSelection();

        if (!this.transactionOriginalList)
            this.transactionOriginalList = this.transactionList;
        else
        {
            this.transactionList = this.transactionOriginalList;
            this.myTransactions = this.transactionList.slice(0,10);
        }

        if (query)
        {
            this.transactionList = this.transactionList.filter(transaction => {
                let transactionName = `${transaction.transactionId}-${transaction.itemId}-${transaction.transactionPIC}`;
                return transactionName.toLowerCase().includes(query);
            });
            this.myTransactions = this.transactionList.slice(0,10);
            this.sortItems();
        }
        else
        {
            this.transactionList = this.transactionOriginalList;
            this.myTransactions = this.transactionList.slice(0,10);
        }
    }

    handleError(error)
    {
        if (error.includes('504'))
        {
            this.router.navigate(['error/504']);
        }
    }

    resetFilter()
    {
        this.activeFilter = {
            itemPIC             : false,
            itemEntrustedStatus : false
        };
    }

    deleteTransaction()
    {
        if (this.deleteTarget)
        {
            this.transactionService.delete(this.deleteTarget).subscribe(
                (response) => {
                    this.resetSelection();
                    this.resetFilter();
                    this.router.navigate(['/admin/transaction/dashboard']);
                    this.snackBar.open(`${response.transactionId}-${response.itemId} berhasil dihapus!`, 'OK', {
                        duration: 1500
                    });
                    this.sharedService.notifyOther({ option: 'refresh' });
                },
                error =>  {
                    this.handleError(error);
                }
            );
        }
    }

    initiateFilter(snackBar:boolean = true, routeHome:boolean = true)
    {
        this.transactionService.getTransactionFiltered(this.activeFilter).subscribe(transactions => {

            let searchForm = <HTMLInputElement>document.getElementById('search');
            searchForm.value = '';

            this.transactionList = transactions;
            this.myTransactions = this.transactionList.slice(0,10);
            this.sortItems();

            if (this.selectedTransaction && this.myTransactions.filter((e)=>e.transactionId === this.selectedTransaction.transactionId).length === 0)
                this.resetSelection(false);

            if (routeHome)
                this.router.navigate(["/admin/transaction/dashboard"]);

            if (snackBar)
            {
                this.snackBar.open(`Filtered Transactions`,'OK',{
                    duration: 1500
                });
            };
        });
    }

    getMoreData()
    {
        if (this.myTransactions.length < this.transactionList.length)
        {
            this.myTransactions = this.transactionList.slice(0,this.myTransactions.length + 5);
        }
    }

    resetSelection(routeHome:boolean = true)
    {
        this.selectedTransaction = undefined;
        this.deleteTarget = undefined;

        if (routeHome)
            this.router.navigate(['/admin/transaction/dashboard']);
    }

    getTransactions()
    {
        this.transactionService.getAll().subscribe
        (
            transactions => {
                this.transactionOriginalList = transactions;
                this.transactionList = transactions;
                this.myTransactions = this.transactionList.slice(0,10);
            },
            error =>  {
                this.handleError(error);
            }
        );
    }

    onCardClick(transaction)
    {
        const transactionId = transaction.transactionId;
        this.selectedTransaction = transaction;
        this.deleteTarget = transaction.itemId;
        this.router.navigate(['admin/transaction/edit', transactionId]);
    }

    openDeleteDialog()
    {
        let config = new MatDialogConfig();
        let dialogRef = this.dialog.open(DeleteDialogComponent, config);

        dialogRef.componentInstance.itemName = `${this.selectedTransaction.transactionId}-${this.selectedTransaction.itemId}`;

        dialogRef.afterClosed().subscribe(result => {
        if (result)
            this.deleteTransaction();
        });
    }
    
    showAddTransactionForm()
    {
        this.resetSelection();
        this.router.navigate(['admin/transaction/add']);
    }

    openFilterDialog()
    {
        let config = new MatDialogConfig();
        let dialogRef = this.dialog.open(TransactionFilterDialogComponent, config);

        dialogRef.componentInstance.checkedPIC = this.activeFilter.itemPIC != false || this.activeFilter.itemPIC == null? true:false;
        dialogRef.componentInstance.itemPIC = this.activeFilter.itemPIC != false || this.activeFilter.itemPIC == null? this.activeFilter.itemPIC: null;
        dialogRef.componentInstance.checkedEntrusted = this.activeFilter.itemEntrustedStatus != false || this.activeFilter.itemEntrustedStatus == null? true:false;
        dialogRef.componentInstance.itemEntrustedStatus = this.activeFilter.itemEntrustedStatus != false || this.activeFilter.itemEntrustedStatus == null? this.activeFilter.itemEntrustedStatus: null;        

        dialogRef.afterClosed().subscribe(result => {
            if (result)
            {
                this.activeFilter = result;
                this.initiateFilter();
            }
        });
    }
}