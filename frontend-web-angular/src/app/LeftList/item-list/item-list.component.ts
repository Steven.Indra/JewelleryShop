import { Component, Inject, Input, OnInit } from '@angular/core';
import { ItemService } from './../../service/item.service';
import { SharedService } from './../../service/shared-service.service';
import { CategoryService } from './../../service/category.service';
import { Router } from '@angular/router';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { DeleteDialogComponent } from './../../dialog/delete-dialog/delete-dialog.component';
import { ItemFilterDialogComponent } from './../../dialog/item-filter-dialog/item-filter-dialog.component';
import { SessionService } from 'app/service/session.service';
import { UtilityToken } from './../../providers';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent {

    selectedItem;
    deleteTarget;
    itemList = [];
    myItems;
    itemOriginalList;
    sortCode = 1;
    activeFilter = {
        itemCategory        : false,
        itemEntrustedStatus : false,
        itemSold            : false 
    };

    categories=[];

    privileges = {
        addItem: false,
        deleteItem: false
    }

    private subscription: Subscription;

    constructor(
        private itemService: ItemService,
        private categoryService: CategoryService,
        private router: Router,
        private sharedService: SharedService,
        public snackBar: MatSnackBar,
        public dialog: MatDialog,
        private sessionService: SessionService,
        @Inject(UtilityToken) public utilityList
    ) 
    {}

    ngOnInit() {

        this.getItems();

        this.checkPrivilege();

        this.getCategories();

        this.subscription = this.sharedService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('option') && res.option === 'refresh') 
            {
                //this.initiateFilter(false, false);
                this.getItems();
            }
            else if (res.hasOwnProperty('option') && res.option === 'highlight')
            {
                if (!this.selectedItem)
                {
                    this.selectedItem = res.value;
                    this.deleteTarget = this.selectedItem.itemId;
                }
            }
            else if (res.hasOwnProperty('option') && res.option === 'unhighlight')
            {
                this.resetSelection();
            }else if (res.hasOwnProperty('option') && res.option === 'loginStatus' && res.value === "login") 
            {
                this.checkPrivilege();
            }else if (res.hasOwnProperty('option') && res.option === 'loginStatus' && res.value === "logout") 
            {
                this.checkPrivilege();
                this.selectedItem = null;
            }
        });
        
    }

    checkPrivilege()
    {
        this.privileges.addItem = this.sessionService.checkPrivilege("Add Item");
        this.privileges.deleteItem = this.sessionService.checkPrivilege("Delete Item");
    }

    getCategories()
    {
        this.categoryService.getAll().subscribe
        (
            categories => {
                this.categories = categories;
            }
        );
    }

    sortItems()
    {
        if (this.sortCode === 0)
        {
            this.myItems.sort(function(a, b)
            {
                return a.itemId - b.itemId;
            });
        }
        else if (this.sortCode === 1)
        {
            this.myItems.sort(function(a, b)
            {
                return a.itemId.localeCompare(b.itemId);
            });
        }
        else if (this.sortCode === 2)
        {
            this.myItems.sort(function(a, b)
            {
                return b.itemId.localeCompare(a.itemId);
            });
        }
    }

    initiateSort()
    {
        let sortMeaning;

        this.sortCode++;
        if (this.sortCode > 2)
        this.sortCode = 1;

        switch (this.sortCode)
        {
        case 0:
            sortMeaning = 'Default';
            break;
        case 1:
            sortMeaning = 'terkecil ke terbesar';
            break;
        case 2:
            sortMeaning = 'terbesar ke terkecil';
            break;
        default:
            break;
        }

        this.resetSelection();
        this.sortItems();

        this.snackBar.open(`Barang diurutkan berdasarkan Id dari ${sortMeaning}`, 'OK',
        {
        duration: 1500,
        });
    }


    initiateSearch($event)
    {
        const query = $event.target.value.toLowerCase();
        this.resetSelection();

        if (!this.itemOriginalList)
            this.itemOriginalList = this.itemList;
        else
        {
            this.itemList = this.itemOriginalList;
            this.myItems = this.itemList.slice(0,10);
        }

        if (query)
        {
            this.itemList = this.itemList.filter(item => {
                let itemName = `${item.itemId}-${item.itemName}`;
                return itemName.toLowerCase().includes(query);
            });
            this.myItems = this.itemList.slice(0,10);
            this.sortItems();
        }
        else
        {
            this.itemList = this.itemOriginalList;
            this.myItems = this.itemList.slice(0,10);
        }
    }

    handleError(error)
    {
        if (error.includes('504'))
        {
        this.router.navigate(['error/504']);
        }
    }

    resetFilter()
    {
        this.activeFilter = {
            itemCategory        : false,
            itemEntrustedStatus : false,
            itemSold            : false 
        };
    }

    deleteItem()
    {
        if (this.deleteTarget)
        {
            this.itemService.delete(this.deleteTarget).subscribe(
                (response) => {
                    this.resetSelection();
                    this.resetFilter();
                    this.router.navigate(['/home/item/']);
                    this.snackBar.open(`${response.itemId}-${response.itemName} berhasil dihapus!`, 'OK', {
                        duration: 1500
                    });
                    this.sharedService.notifyOther({ option: 'refresh' });
                },
                error =>  {
                    this.handleError(error);
                }
            );
        }
    }

    initiateFilter(snackBar:boolean = true, routeHome:boolean = true)
    {
        this.itemService.getItemsFiltered(this.activeFilter).subscribe(items => {

            let searchForm = <HTMLInputElement>document.getElementById('search');
            searchForm.value = '';

            this.itemList = items;
            this.myItems = this.itemList.slice(0,10);
            this.sortItems();

            if (this.selectedItem && this.myItems.filter((e)=>e.itemId === this.selectedItem.itemId).length === 0)
                this.resetSelection(false);

            if (routeHome)
                this.router.navigate(["/"]);

            if (snackBar)
            {
                this.snackBar.open(`Filtered items`,'OK',{
                    duration: 1500
                });
            };
        });
    }

    getMoreData()
    {
        if (this.myItems.length < this.itemList.length)
        {
            this.myItems = this.itemList.slice(0,this.myItems.length + 5);
        }
    }

    resetSelection(routeHome:boolean = true)
    {
        this.selectedItem = undefined;
        this.deleteTarget = undefined;

        if (routeHome)
            this.router.navigate(['/']);
    }

    getItems()
    {
        this.itemService.getItems().subscribe
        (
            items => {
                this.itemOriginalList = items;
                this.itemList = items;
                this.myItems = this.itemList.slice(0,10);
            },
            error =>  {
                this.handleError(error);
            }
        );
    }

    showAddItemForm()
    {
        this.resetSelection();
        this.router.navigate(['home/item/add']);
    }

    onCardClick(item)
    {
        const itemId = item.itemId;
        this.selectedItem = item;
        this.deleteTarget = itemId;
        this.router.navigate(['home/item/edit', itemId]);
    }

    openDeleteDialog()
    {
        let config = new MatDialogConfig();
        let dialogRef = this.dialog.open(DeleteDialogComponent, config);

        dialogRef.componentInstance.itemName = `${this.selectedItem.itemId}-${this.selectedItem.itemName}`;

        dialogRef.afterClosed().subscribe(result => {
        if (result)
            this.deleteItem();
        });
    }
    
    openFilterDialog()
    {
        let config = new MatDialogConfig();
        let dialogRef = this.dialog.open(ItemFilterDialogComponent, config);

        dialogRef.componentInstance.categories = this.categories;
        dialogRef.componentInstance.soldStatusOption = this.utilityList.soldStatus;
        dialogRef.componentInstance.checkedCategory = this.activeFilter.itemCategory? true:false;
        dialogRef.componentInstance.itemCategory = this.activeFilter.itemCategory? this.activeFilter.itemCategory:null;
        dialogRef.componentInstance.checkedEntrusted = this.activeFilter.itemEntrustedStatus != false || this.activeFilter.itemEntrustedStatus == null? true:false;
        dialogRef.componentInstance.itemEntrustedStatus = this.activeFilter.itemEntrustedStatus != false || this.activeFilter.itemEntrustedStatus == null? this.activeFilter.itemEntrustedStatus: null;
        dialogRef.componentInstance.checkedSold = this.activeFilter.itemSold? true:false;
        dialogRef.componentInstance.itemSoldStatus = this.activeFilter.itemSold? this.activeFilter.itemSold:null;
        

        dialogRef.afterClosed().subscribe(result => {
            if (result)
            {
                this.activeFilter = result;
                this.initiateFilter();
            }
        });
    }
}