import { Component, Inject, Input, OnInit } from '@angular/core';
import { CategoryService } from './../../service/category.service';
import { SharedService } from './../../service/shared-service.service';
import { Router } from '@angular/router';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { UtilityToken } from './../../providers';
import { Subscription } from 'rxjs/Subscription';
import { DeleteDialogComponent } from './../../dialog/delete-dialog/delete-dialog.component';
import { SessionService } from 'app/service/session.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent {

    categoryList = [];
    myItems;
    categoryOriginalList;
    selectedItem;
    deleteTarget;
    sortCode = 0;
    private subscription: Subscription;
    constructor(
        private router: Router,
        private categoryService: CategoryService,
        public dialog: MatDialog,
        public snackBar: MatSnackBar,
        private sharedService: SharedService,
        private sessionService: SessionService
    ) 
    {}

    privileges = {
        addCategory: false,
        deleteCategory: false
    };

    ngOnInit() {
        this.getCategories();
        this.checkPrivilege();
        this.subscription = this.sharedService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('option') && res.option === 'refreshCategory') 
            {
                this.getCategories();
            }else if (res.hasOwnProperty('option') && res.option === 'highlight')
            {
                if (!this.selectedItem)
                {
                    this.selectedItem = res.value;
                    this.deleteTarget = this.selectedItem.categoryId;
                }
            }
            else if (res.hasOwnProperty('option') && res.option === 'unhighlight')
            {
                this.resetSelection();
            }
        });
    }

    getCategories()
    {
        this.categoryService.getAll().subscribe
        (
            categories => {
                this.categoryOriginalList = categories;
                this.categoryList = categories;
                this.myItems = this.categoryList.slice(0,10);
            }
        );
    }

    checkPrivilege()
    {
        this.privileges.addCategory = this.sessionService.checkPrivilege("Add Category");
        this.privileges.deleteCategory = this.sessionService.checkPrivilege("Delete Category");
    }

    showAddCategoryForm()
    {
        this.resetSelection();
        this.router.navigate(['admin/category/add']);
    }
    
    backToAdminDashboard()
    {
        this.router.navigate(['admin/dashboard']);
    }

    onCardClick(categoryItem)
    {
        const categoryId = categoryItem.categoryId;
        this.selectedItem = categoryItem;
        this.deleteTarget = categoryId;
        this.router.navigate(['admin/category/edit', categoryId]);
    }

    initiateSearch($event)
    {
        const query = $event.target.value.toLowerCase();
        this.resetSelection();

        if (!this.categoryOriginalList)
            this.categoryOriginalList = this.categoryList;
        else
        {
            this.categoryList = this.categoryOriginalList;
            this.myItems = this.categoryList.slice(0,10);
        }

        if (query)
        {
            this.categoryList = this.categoryList.filter(category => {
                let categoryName = `${category.categoryName}`;
                return categoryName.toLowerCase().includes(query);
            });
            this.myItems = this.categoryList.slice(0,10);
            this.sortItems();
        }
        else
        {
            this.categoryList = this.categoryOriginalList;
            this.myItems = this.categoryList.slice(0,10);
        }
    }

    sortItems()
    {
        if (this.sortCode === 0)
        {
            this.myItems.sort(function(a, b)
            {
                return a.categoryId - b.categoryId;
            });
        }
        else if (this.sortCode === 1)
        {
            this.myItems.sort(function(a, b)
            {
                return a.categoryName.localeCompare(b.categoryName);
            });
        }
        else if (this.sortCode === 2)
        {
            this.myItems.sort(function(a, b)
            {
                return b.categoryName.localeCompare(a.categoryName);
            });
        }
    }

    initiateSort()
    {
        let sortMeaning;

        this.sortCode++;
        if (this.sortCode > 2)
            this.sortCode = 0;

        switch (this.sortCode)
        {
            case 0:
                sortMeaning = 'Default';
                break;
            case 1:
                sortMeaning = 'A ke Z';
                break;
            case 2:
                sortMeaning = 'Z ke A';
                break;
            default:
                break;
        }

        this.resetSelection();
        this.sortItems();

        this.snackBar.open(`Nama Kategori diurutkan dari ${sortMeaning}`, 'OK',
        {
            duration: 1500,
        });
    }

    resetSelection(routeHome:boolean = true)
    {
        this.selectedItem = undefined;
        this.deleteTarget = undefined;

        if (routeHome)
            this.router.navigate(['/admin/category/dashboard']);
    }

    handleError(error)
    {
        if (error.includes('504'))
        {
        this.router.navigate(['error/504']);
        }
    }

    deleteItem()
    {
        if (this.deleteTarget)
        {
        this.categoryService.delete(this.deleteTarget).subscribe(
            (response) => {
                this.resetSelection();
                this.router.navigate(['/admin/category/dashboard']);
                this.snackBar.open(`${response.categoryName} berhasil dihapus!`, 'OK', {
                    duration: 1500
                });
                this.sharedService.notifyOther({ option: 'refreshCategory' });
            },
            error =>  {
                this.handleError(error);
            }
        );
        }
    }

    openDeleteDialog()
    {
        let config = new MatDialogConfig();
        let dialogRef = this.dialog.open(DeleteDialogComponent, config);

        dialogRef.componentInstance.itemName = `${this.selectedItem.categoryName}`;

        dialogRef.afterClosed().subscribe(result => {
        if (result)
            this.deleteItem();
        });
    }
}