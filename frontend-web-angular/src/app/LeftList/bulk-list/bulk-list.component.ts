import { Component, Inject, Input, OnInit } from '@angular/core';
import { CategoryService } from './../../service/category.service';
import { SharedService } from './../../service/shared-service.service';
import { Router } from '@angular/router';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { UtilityToken } from './../../providers';
import { Subscription } from 'rxjs/Subscription';
import { DeleteDialogComponent } from './../../dialog/delete-dialog/delete-dialog.component';
import { SessionService } from 'app/service/session.service';
import { ErrorDialogComponent } from './../../dialog/error-dialog/error-dialog.component';

@Component({
  selector: 'app-bulk-list',
  templateUrl: './bulk-list.component.html',
  styleUrls: ['./bulk-list.component.css']
})
export class BulkListComponent {

    selectedBulk;
    bulkList = ["Barang"];
    userLoggedData;
    private subscription: Subscription;
    constructor(
        private router: Router,
        private sessionService: SessionService,
        private sharedService: SharedService,
        public dialog: MatDialog,
    ) 
    {}

    ngOnInit() {
        this.checkPath();
        this.subscription = this.sharedService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('option') && res.option === 'highlight')
            {
                if (!this.selectedBulk)
                {
                    this.selectedBulk = res.value;
                }
            }
            else if (res.hasOwnProperty('option') && res.option === 'unhighlight')
            {
                this.resetSelection();
            }
        });
    }

    checkPath()
    {
        if (this.router.url.indexOf('bulk/item') >= 0)
        {
            this.checkSelectedBulk("Item");
        }
    }

    checkSelectedBulk(bulkItem)
    {
        this.selectedBulk = bulkItem;
    }

    resetSelection(routeHome:boolean = true)
    {
        this.selectedBulk = undefined;

        if (routeHome)
            this.router.navigate(['/admin/bulk/dashboard']);
    }

    onBulkItemClick(bulkItem)
    {
        this.selectedBulk = bulkItem;
        if (bulkItem == "Barang")
        {
            this.router.navigate(['admin/bulk/item']);
        }
    }

    backToAdminDashboard()
    {
        this.router.navigate(['admin/dashboard']);
    }

    openErrorDialog(errorTitle, errorMessage)
    {
      let config = new MatDialogConfig();
      let dialogRef = this.dialog.open(ErrorDialogComponent, config);
  
      dialogRef.componentInstance.errorTitle = errorTitle;
      dialogRef.componentInstance.errorMessage = errorMessage;
    }
}