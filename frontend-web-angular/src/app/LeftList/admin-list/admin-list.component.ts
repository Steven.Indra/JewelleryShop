import { Component, Inject, Input, OnInit } from '@angular/core';
import { ItemService } from './../../service/item.service';
import { SharedService } from './../../service/shared-service.service';
import { Router } from '@angular/router';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { UtilityToken } from './../../providers';
import { Subscription } from 'rxjs/Subscription';
import { SessionService } from 'app/service/session.service';
import { ErrorDialogComponent } from './../../dialog/error-dialog/error-dialog.component';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent implements OnInit {

    adminList = ["Kategori","User","Transaksi","Laporan","Upload"];
    userLoggedData;
    constructor(
        private router: Router,
        private sessionService: SessionService,
        public dialog: MatDialog,
    ) 
    {}

    ngOnInit() {
        
    }

    onAdminItemClick(adminItem)
    {
        if (adminItem == "Kategori")
        {
            if (this.sessionService.checkPrivilege("Category Dashboard"))
            {
                this.router.navigate(['admin/category/dashboard']);
            }else
            {
                this.openErrorDialog("Insufficient Privilege", "Kamu tidak punya Akses ke Beranda Kategori");
            }
        }else if (adminItem == "Transaksi")
        {
            if (this.sessionService.checkPrivilege("Transaction Dashboard"))
            {
                this.router.navigate(['admin/transaction/dashboard']);
            }else
            {
                this.openErrorDialog("Insufficient Privilege", "Kamu tidak punya Akses ke Beranda Transaksi");
            }
        }else if (adminItem == "User")
        {
            if (this.sessionService.checkPrivilege("User Dashboard"))
            {
                this.router.navigate(['admin/user/dashboard']);
            }else
            {
                this.openErrorDialog("Insufficient Privilege", "Kamu tidak punya Akses ke Beranda User");
            }
        }else if (adminItem == "Laporan")
        {
            if (this.sessionService.checkPrivilege("Report Dashboard"))
            {
                this.router.navigate(['admin/report/dashboard']);
            }else
            {
                this.openErrorDialog("Insufficient Privilege", "Kamu tidak punya Akses ke Beranda Laporan");
            }
        }else if (adminItem == "Upload")
        {
            if (this.sessionService.checkPrivilege("Bulk Dashboard"))
            {
                this.router.navigate(['admin/bulk/dashboard']);
            }else
            {
                this.openErrorDialog("Insufficient Privilege", "Kamu tidak punya Akses ke Beranda Upload");
            }
        }
    }

    backToHome()
    {
        this.router.navigate(['home']);
    }

    openErrorDialog(errorTitle, errorMessage)
    {
      let config = new MatDialogConfig();
      let dialogRef = this.dialog.open(ErrorDialogComponent, config);
  
      dialogRef.componentInstance.errorTitle = errorTitle;
      dialogRef.componentInstance.errorMessage = errorMessage;
    }
}