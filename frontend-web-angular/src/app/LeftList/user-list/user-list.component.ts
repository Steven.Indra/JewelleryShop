import { Component, Inject, Input, OnInit } from '@angular/core';
import { UserService } from './../../service/user.service';
import { SharedService } from './../../service/shared-service.service';
import { Router } from '@angular/router';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { UtilityToken } from './../../providers';
import { Subscription } from 'rxjs/Subscription';
import { DeleteDialogComponent } from './../../dialog/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent {

    userList = [];
    myUser;
    userOriginalList;
    selectedItem;
    deleteTarget;
    sortCode = 1;
    private subscription: Subscription;
    constructor(
        private router: Router,
        private userService: UserService,
        public dialog: MatDialog,
        public snackBar: MatSnackBar,
        private sharedService: SharedService
    ) 
    {}

    ngOnInit() {
        this.getAllUserWithoutAdmin();
        this.subscription = this.sharedService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('option') && res.option === 'refreshUser') 
            {
                this.getAllUserWithoutAdmin();
            }else if (res.hasOwnProperty('option') && res.option === 'highlight')
            {
                if (!this.selectedItem)
                {
                    this.selectedItem = res.value;
                    this.deleteTarget = this.selectedItem.userId;
                }
            }
            else if (res.hasOwnProperty('option') && res.option === 'unhighlight')
            {
                this.resetSelection();
            }
        });
    }

    getAllUserWithoutAdmin()
    {
        this.userService.getAllWithoutAdmin().subscribe
        (
            users => {
                this.userOriginalList = users;
                this.userList = users;
                this.myUser = this.userList.slice(0,10);
            }
        );
    }

    showAddUserForm()
    {
        this.resetSelection();
        this.router.navigate(['admin/user/add']);
    }
    
    backToAdminDashboard()
    {
        this.router.navigate(['admin/dashboard']);
    }

    onCardClick(userItem)
    {
        const userId = userItem.userId;
        this.selectedItem = userItem;
        this.deleteTarget = userId;
        this.router.navigate(['admin/user/edit', userId]);
    }

    initiateSearch($event)
    {
        const query = $event.target.value.toLowerCase();
        this.resetSelection();

        if (!this.userOriginalList)
            this.userOriginalList = this.userList;
        else
        {
            this.userList = this.userOriginalList;
            this.myUser = this.userList.slice(0,10);
        }

        if (query)
        {
            this.userList = this.userList.filter(user => {
                let userName = `${user.userName}-${user.userShownName}`;
                return userName.toLowerCase().includes(query);
            });
            this.myUser = this.userList.slice(0,10);
            this.sortItems();
        }
        else
        {
            this.userList = this.userOriginalList;
            this.myUser = this.userList.slice(0,10);
        }
    }

    sortItems()
    {
        if (this.sortCode === 0)
        {
            this.myUser.sort(function(a, b)
            {
                return a.userId - b.userId;
            });
        }
        else if (this.sortCode === 1)
        {
            this.myUser.sort(function(a, b)
            {
                return a.userShownName.localeCompare(b.userShownName);
            });
        }
        else if (this.sortCode === 2)
        {
            this.myUser.sort(function(a, b)
            {
                return b.userShownName.localeCompare(a.userShownName);
            });
        }
    }

    initiateSort()
    {
        let sortMeaning;

        this.sortCode++;
        if (this.sortCode > 2)
            this.sortCode = 1;

        switch (this.sortCode)
        {
            case 0:
                sortMeaning = 'Default';
                break;
            case 1:
                sortMeaning = 'A ke Z';
                break;
            case 2:
                sortMeaning = 'Z ke A';
                break;
            default:
                break;
        }

        this.resetSelection();
        this.sortItems();

        this.snackBar.open(`Nama user diurutkan dari ${sortMeaning}`, 'OK',
        {
            duration: 1500,
        });
    }

    resetSelection(routeHome:boolean = true)
    {
        this.selectedItem = undefined;
        this.deleteTarget = undefined;

        if (routeHome)
            this.router.navigate(['/admin/user/dashboard']);
    }

    handleError(error)
    {
        if (error.includes('504'))
        {
        this.router.navigate(['error/504']);
        }
    }

    deleteItem()
    {
        if (this.deleteTarget)
        {
        this.userService.delete(this.deleteTarget).subscribe(
            (response) => {
                this.resetSelection();
                this.router.navigate(['/admin/user/dashboard']);
                this.snackBar.open(`${response.userShownName} berhasil dihapus!`, 'OK', {
                    duration: 1500
                });
                this.sharedService.notifyOther({ option: 'refreshUser' });
            },
            error =>  {
                this.handleError(error);
            }
        );
        }
    }

    openDeleteDialog()
    {
        let config = new MatDialogConfig();
        let dialogRef = this.dialog.open(DeleteDialogComponent, config);

        dialogRef.componentInstance.itemName = `${this.selectedItem.userShownName}`;

        dialogRef.afterClosed().subscribe(result => {
        if (result)
            this.deleteItem();
        });
    }
}