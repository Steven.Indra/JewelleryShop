import { Routes, RouterModule } from '@angular/router';

import { ItemFormComponent } from './form/item-form/item-form.component';
import { CategoryFormComponent } from './form/category-form/category-form.component';
import { TransactionFormComponent } from './form/transaction-form/transaction-form.component';
import { UserFormComponent } from './form/user-form/user-form.component';
import { WelcomeMessageComponent } from './welcome-message/welcome-message.component';
import { ItemErrorComponent } from './item-error/item-error.component';
import { ItemListComponent } from './leftlist/item-list/item-list.component';
import { AdminListComponent } from './leftlist/admin-list/admin-list.component';
import { CategoryListComponent } from './leftlist/category-list/category-list.component';
import { TransactionListComponent } from './leftlist/transaction-list/transaction-list.component';
import { ReportListComponent } from './leftlist/report-list/report-list.component';
import { UserListComponent } from './leftlist/user-list/user-list.component';
import { ItemReportComponent } from './report/item-report/item-report.component';
import { TransactionReportComponent } from './report/transaction-report/transaction-report.component';
import { AuthGuard } from './authentication/auth-guard.service';
import { BulkListComponent } from './leftlist/bulk-list/bulk-list.component';
import { BulkItemComponent } from './bulk/item/bulk-item.component';

const appRoutes: Routes = [
    {   path: '', redirectTo: "/home", pathMatch:"full"},
    {   path: 'admin', redirectTo: "/admin/dashboard", pathMatch:"full"},
    {   path: 'home/item', redirectTo: "/home", pathMatch:"full"},
    {   path: 'home/item/edit', redirectTo: "/home", pathMatch:"full"},
    {   path: 'admin/category', redirectTo: "/admin/category/dashboard", pathMatch:"full"},
    {   path: 'admin/category/edit', redirectTo: "/admin/category/dashboard", pathMatch:"full"},
    {   path: 'admin/transaction', redirectTo: "/admin/transaction/dashboard", pathMatch:"full"},
    {   path: 'admin/transaction/edit', redirectTo: "/admin/transaction/dashboard", pathMatch:"full"},
    {   path: 'admin/user', redirectTo: "/admin/user/dashboard", pathMatch:"full"},
    {   path: 'admin/user/edit', redirectTo: "/admin/user/dashboard", pathMatch:"full"},
    {   path: 'admin/report', redirectTo: "/admin/report/dashboard", pathMatch:"full"},
    {   path: 'error/:error_code',
        children: [
            {   path: '', component: ItemListComponent, outlet: 'aux' },
            {   path: '', component: ItemErrorComponent }]
    },
    {   path: 'home',
        children: [
            {   path: '', component: ItemListComponent, outlet: 'aux' },
            {   path: '', component: WelcomeMessageComponent },
            {   path: 'item',
                canActivateChild: [AuthGuard], 
                children: [
                    {   path: 'add', component: ItemFormComponent },
                    {   path: 'edit/:item_id', component: ItemFormComponent }]
            }]
    },
    {   path: 'admin',
        canActivateChild: [AuthGuard], 
        children: [
            {   path: '', component: AdminListComponent, outlet: 'aux' },
            {   path: 'dashboard', component: WelcomeMessageComponent }]
    },
    {   path: 'admin/category',
        canActivateChild: [AuthGuard], 
        children: [
            {   path: '', component: CategoryListComponent, outlet: 'aux' },
            {   path: 'dashboard', component: WelcomeMessageComponent },
            {   path: 'add', component: CategoryFormComponent },
            {   path: 'edit/:category_id', component: CategoryFormComponent }]
    },
    {   path: 'admin/transaction',
        canActivateChild: [AuthGuard], 
        children: [
            {   path: '', component: TransactionListComponent, outlet: 'aux' },
            {   path: 'dashboard', component: WelcomeMessageComponent },
            {   path: 'add', component: TransactionFormComponent },
            {   path: 'edit/:transaction_id', component: TransactionFormComponent }]
    },
    {   path: 'admin/user',
        canActivateChild: [AuthGuard], 
        children: [
            {   path: '', component: UserListComponent, outlet: 'aux' },
            {   path: 'dashboard', component: WelcomeMessageComponent },
            {   path: 'add', component: UserFormComponent },
            {   path: 'edit/:user_id', component: UserFormComponent }
        ]
    },
    {   path: 'admin/report',
        canActivateChild: [AuthGuard], 
        children: [
            {   path: '', component: ReportListComponent, outlet: 'aux' },
            {   path: 'dashboard', component: WelcomeMessageComponent },
            {   path: 'stockitem', component: ItemReportComponent },
            {   path: 'incomingitem', component: ItemReportComponent },
            {   path: 'exititem', component: ItemReportComponent },
            {   path: 'transaction', component: TransactionReportComponent }]
    },
    {   path: 'admin/bulk',
        canActivateChild: [AuthGuard], 
        children: [
            {   path: '', component: BulkListComponent, outlet: 'aux' },
            {   path: 'dashboard', component: WelcomeMessageComponent },
            {   path: 'item', component: BulkItemComponent }]
    }
];

export const routing = RouterModule.forRoot(appRoutes);