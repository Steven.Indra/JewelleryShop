import { Component, Inject, Input, OnInit } from '@angular/core';
import { SharedService } from './service/shared-service.service';
import { SessionService } from './service/session.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { UtilityToken } from './providers';
import { Subscription } from 'rxjs/Subscription';
import { LoginDialogComponent } from './dialog/login-dialog/login-dialog.component';
import { LogoutDialogComponent } from './dialog/logout-dialog/logout-dialog.component';
import { ChangePasswordDialogComponent } from './dialog/change-password-dialog/change-password-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  loggedUser;
  userLoggedIn;
  userLoggedData;
  constructor(
      private router: Router,
      private sharedService: SharedService,
      private sessionService: SessionService,
      public snackBar: MatSnackBar,
      public dialog: MatDialog,
      private activeRoute: ActivatedRoute
  ) 
  {
    
  }

  ngOnInit() {
    document.getElementById("sidebar-cover").addEventListener("click", this.hideSidebar);

    this.userLoggedData = this.sessionService.retrieveSession();
    if (this.userLoggedData)
    {
      this.loggedUser = this.userLoggedData.userShownName;
      this.userLoggedIn = true;
    }else
    {
      this.loggedUser = "Belum Masuk";
      this.userLoggedIn = false;
    }

  }

  toggleMenu()
  {
      var width = window.innerWidth;

      var sideMenu = document.getElementsByClassName("sidebar")[0];
      var sideMenuCover = document.getElementById("sidebar-cover");

      sideMenu.classList.toggle("shown");
      sideMenuCover.classList.toggle("shown");
  }

  hideSidebar()
  {
    const sidebar = document.getElementsByClassName("sidebar")[0];
    const sidebarCover = document.getElementById('sidebar-cover');

    if (sidebar.classList.contains('shown')) 
    {
      sidebar.classList.toggle('shown');
      sidebarCover.classList.toggle('shown');
    }
  }

  logout()
  {
    this.sessionService.removeSession();
    this.userLoggedData = null;
    this.userLoggedIn = false;
    this.loggedUser = "Belum Masuk";
    if (this.router.url.indexOf("admin") >=0 || this.router.url.indexOf("add") >=0 || this.router.url.indexOf("dashboard") >=0 || this.router.url.indexOf("edit") >=0)
    {
      this.router.navigate(["/home"]);
    }
    this.sharedService.notifyOther({ option: 'loginStatus', value: "logout" });
  }

  login(userLoggedIn)
  {
    this.sessionService.createSession(userLoggedIn);
    this.userLoggedData = userLoggedIn;
    this.userLoggedIn = true;
    this.loggedUser = userLoggedIn.userShownName;
    this.sharedService.notifyOther({ option: 'loginStatus', value: "login" });
  }

  backToHome()
  {
    this.hideSidebar();
    this.router.navigate(["/home"]);
  }

  changePassword(userLoggedData)
  {
    this.sessionService.renewSessionData(userLoggedData);
    this.userLoggedData = userLoggedData;
    this.userLoggedIn = true;
  }

  openChangePasswordDialog()
  {
    this.hideSidebar();
    let config = new MatDialogConfig();
    let dialogRef = this.dialog.open(ChangePasswordDialogComponent, config);

    dialogRef.componentInstance.userLoggedData =  this.userLoggedData;
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.changePassword(result.userLoggedData);
    });
  }

  openLogoutDialog()
  {
    this.hideSidebar();
    let config = new MatDialogConfig();
    let dialogRef = this.dialog.open(LogoutDialogComponent, config);

    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.logout();
    });
  }

  openLoginDialog()
  {
    this.hideSidebar();
    let config = new MatDialogConfig();
    let dialogRef = this.dialog.open(LoginDialogComponent, config);

    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.login(result.userLoggedIn);
    });
  }

  openAdminHome()
  {
    this.hideSidebar();
    this.router.navigate(['admin/dashboard']);
  }
}
