import { Directive, HostListener, ElementRef, Input } from "@angular/core";
import { DecimalPipe } from "@angular/common";
import { NgControl } from "@angular/forms";

@Directive({ 
  selector: "[ngModel][CurrencyFormatter]",
  host:{
      '(ngModelChange)': 'onInputChange($event)'
  }
})
export class CurrencyFormatterDirective {

  private el: HTMLInputElement;

  constructor(
    private elementRef: ElementRef,
    private decimalPipe: DecimalPipe,
    public model: NgControl
  ) {
    this.el = this.elementRef.nativeElement;
  }

  onInputChange(event){    
    var valArray;
    if (event != null)
      {
        valArray= event.toString().split('.');
      }
     else
      {
        valArray = '';
      }
    for(var i = 0; i < valArray.length; ++i) {
      valArray[i] = valArray[i].replace(/\D/g, '');
    }

    var newVal: String;

    if(valArray.length === 0) {
      newVal = '';
    }
    else {
      let matches = valArray[0].match(/[0-9]{3}/mig);

      if(matches !== null && valArray[0].length > 3) {
        let commaGroups = Array.from(Array.from(valArray[0]).reverse().join('').match(/[0-9]{3}/mig).join()).reverse().join('');
        let replacement = valArray[0].replace(commaGroups.replace(/\D/g, ''), '');

        newVal = (replacement.length > 0 ? replacement + "," : "") + commaGroups;
      } else {
        newVal = valArray[0];
      }

      if(valArray.length > 1) {
        newVal += "." + valArray[1].substring(0,2);
      }
    }
    // set the new value
    newVal = this.decimalPipe.transform(newVal.replace(new RegExp("[,]", "g"), ""),'1.0-0' );
    this.model.valueAccessor.writeValue(newVal);
  }
}