import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogConfig, MatDialogRef, MatSnackBar, MatDialog, MatCheckbox } from '@angular/material';
import { ItemService } from './../../service/item.service';
import { TransactionService } from './../../service/transaction.service';
import { CategoryService } from './../../service/category.service';
import { SharedService } from './../../service/shared-service.service';
import { UtilityToken } from './../../providers';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe, DecimalPipe } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { ErrorDialogComponent } from './../../dialog/error-dialog/error-dialog.component';
import * as moment from 'moment';
import 'date-input-polyfill';
import { SessionService } from 'app/service/session.service';
import { ExcelService } from 'app/service/excel.service';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
import * as Papa from 'papaparse/papaparse.min.js';
import { ImportResultDialogComponent } from 'app/dialog/import-result-dialog/import-result-dialog.component';


@Component({
  selector: 'app-bulk-item',
  templateUrl: './bulk-item.component.html',
  styleUrls: ['./bulk-item.component.css']
})
export class BulkItemComponent implements OnInit {

    yearList;
    selectedYear = (new Date()).getFullYear().toString();
    selectedMonth = (new Date()).getMonth() + 1;
    userLoggedIn = false;
    checkedMonth = false;
    selectedReport;
    privileges = {
        viewReport: false
    };
    entrustedOption = 0;
    totalItem = 0;
    totalWeight= 0;
    myFile;
    importData;

    @ViewChild(MatPaginator) paginator : MatPaginator;
    displayedColumnsImport = ['itemId', 'itemName', 'itemCategory','itemWeight','itemGoldContent','itemBuyDate','itemBuyPrice','itemEntrustedStatus'];
    itemRetrievedData;
    dataSource: ItemDataSource | null;

    ngOnInit() {
    }

    private subscription: Subscription;
    constructor(
        private itemService: ItemService,
        private transactionService: TransactionService,
        private categoryService: CategoryService,
        private sharedService: SharedService,
        private activatedRoute: ActivatedRoute,
        public snackBar: MatSnackBar,
        private router: Router,
        private datePipe: DatePipe,
        public dialog: MatDialog,
        private sessionService: SessionService,
        private excelService: ExcelService,
        @Inject(UtilityToken) public utilityList
    )
    {
        
        
    }

    setFile(event)
    {
      const reader = new FileReader();
      const allowedTypes = ['application/vnd.ms-excel'];
      const file = event.target.files[0];

      if (file)
      {
        if ((file.type === allowedTypes[0]) && file.size <= 266240)
        {
          this.myFile = file;
          this.parse();
        }
      }
    }

    parse()
    {
        this.excelService.importExcel(this.myFile, (response)=> { 
            this.importData = response;
            this.fetchData(this.importData);
        });
    }
    
    fetchData(importData)
    {
        let item = importData;
        this.totalWeight = 0;
        this.totalItem = item.length;
        item.forEach(element => {
            this.totalWeight = Math.round(this.totalWeight * 10000) / 10000  + Math.round(element.itemWeight * 10000) / 10000;
        });
        this.showTable();
    }

    downloadImportFormat()
    {
        this.excelService.downloadImportFormatExcel();
    }

    sendImport()
    {
        let successList = [];
        let failureList = [];
        this.importData.forEach((element,index,array) => {
            let formData:any = new FormData();
            formData.append("itemId",element.itemId);
            formData.append("itemName",element.itemName);
            formData.append("itemCategoryName",element.categoryName);
            formData.append("itemWeight",element.itemWeight);
            formData.append("itemGoldContent",element.itemGoldContent);
            formData.append("itemBuyDate",element.itemBuyDate);
            formData.append("itemBuyPrice",element.itemBuyPrice);
            formData.append("itemEntrustedStatus",element.itemEntrustedStatus);
            formData.append("itemNote",element.itemNote);

            this.itemService.addImport(formData).subscribe(response => {
                successList.push(element.itemId);
                this.checkEnd(index,array, successList, failureList);
            },
            error =>  {
                failureList.push(element.itemId + " = " + this.handleError(error));
                this.checkEnd(index,array, successList, failureList);
            });            
        });
    }

    checkEnd(index, array, successList, failureList)
    {
        if (index == array.length -1)
        {
            this.sharedService.notifyOther({ option: 'unhighlight', value: '' });
            this.router.navigate(["/admin/bulk/dashboard"]);
            this.openImportResultDialog(successList, failureList);
        }
    }

    sendCancel()
    {
        this.router.navigate(['/admin/bulk/dashboard']);
        this.sharedService.notifyOther({ option: 'unhighlight', value: '' });
    }

    showTable()
    {
        this.itemRetrievedData = new ItemRetrieve(this.importData);
        this.dataSource = new ItemDataSource(this.itemRetrievedData,this.paginator);
    }

    openImportResultDialog(successList, failureList)
    {
        let config = new MatDialogConfig();
        let dialogRef = this.dialog.open(ImportResultDialogComponent, config);
    
        dialogRef.componentInstance.successList = successList;
        dialogRef.componentInstance.failureList = failureList;
    }

    handleError(error)
    {
      if (error.includes('404'))
        this.router.navigate(['error/404']);
      else if (error.includes('504'))
      {
        this.router.navigate(['error/504']);
      }
      else if (error.includes('415'))
      {
        let errorTitle = `Unsupported Media Type`;
        let errorMessage = `Hanya menerima JPG dan PNG untuk gambar profile`;
        return errorMessage;
      }
      else if (error.includes('406'))
      {
        let errorTitle = `Category not found`;
        let errorMessage = `Kategori tidak ditemukan`;
        return errorMessage;
      }
      else if (error.includes('409'))
      {
        let errorTitle = `Unique`;
        let errorMessage = `Id barang sudah digunakan`;
        return errorMessage;
      }
      else
      {
        let errorTitle = `Generic Error`;
        let errorMessage = `Ada yang salah dengan requestmu. Kontak admin sistem dan beritahu error ini:
        ${error}`;
        return errorMessage;
      }
    }
}

export interface Item {
    itemId: String;
    itemName:String;
    itemWeight : null,
    itemGoldContent: null,
    itemBuyPrice: null,
    itemBuyDate: null,
    itemPhoto: null,
    categoryName: null,
    itemEntrustedStatus:null,
    itemNote:null
}

/** An example database that the data source uses to retrieve data for the table. */
export class ItemRetrieve {
/** Stream that emits whenever the data has been modified. */
    dataChange: BehaviorSubject<Item[]> = new BehaviorSubject<Item[]>([]);
    get data(): Item[] { return this.dataChange.value; }

    constructor(private item:Item) {
    // Fill up the database with 100 users.
        this.addItem(item);  
    }

    addItem(item)
    {
        console.log(item, this.dataChange)
        for (let i=0;i<item.length;i++)
        {
            const copiedData = this.data.slice();
            copiedData.push(item[i]);
            this.dataChange.next(copiedData);
        }
    }

    getLength()
    {
        return this.dataChange.value.length;
    }
}

/**
* Data source to provide what data should be rendered in the table. Note that the data source
* can retrieve its data in any way. In this case, the data source is provided a reference
* to a common data base, ExampleDatabase. It is not the data source's responsibility to manage
* the underlying data. Instead, it only needs to take the data and send the table exactly what
* should be rendered.
*/
export class ItemDataSource extends DataSource<any> {
    constructor(private itemRetrievedData: ItemRetrieve, private _paginator: MatPaginator) {
    super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<Item[]> {
        const displayDataChanges = [
            this.itemRetrievedData.dataChange,
            this._paginator.page
        ];
        return Observable.merge(...displayDataChanges).map(() => {
            const data = this.itemRetrievedData.data.slice();

            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        })
    //return this.itemRetrievedData.dataChange;
    }

    disconnect() {}
}