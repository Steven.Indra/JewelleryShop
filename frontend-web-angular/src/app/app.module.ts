import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
    MatInputModule,
    MatTooltipModule,
    MatSelectModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatTabsModule,
    MatChipsModule,
    MatDialogModule,
    MatSnackBarModule,
    MatMenuModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTableModule,
    MatPaginatorModule
} from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';
import { DatePipe, CurrencyPipe, DecimalPipe } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';
import { AppComponent } from './app.component';
import { UtilityList, UtilityToken } from './providers';
import { CategoryService } from './service/category.service';
import { ItemService } from './service/item.service';
import { TransactionService } from './service/transaction.service';
import { SharedService } from './service/shared-service.service';
import { UserService } from './service/user.service';
import { SessionService } from './service/session.service';
import { PrivilegeService } from './service/privilege.service';
import { ItemCardComponent } from './card/item-card/item-card.component';
import { CategoryCardComponent } from './card/category-card/category-card.component';
import { TransactionCardComponent } from './card/transaction-card/transaction-card.component';
import { ReportCardComponent } from './card/report-card/report-card.component';
import { AdminCardComponent } from './card/admin-card/admin-card.component';
import { UserCardComponent } from './card/user-card/user-card.component';
import { ItemFormComponent } from './form/item-form/item-form.component';
import { CategoryFormComponent } from './form/category-form/category-form.component';
import { TransactionFormComponent } from './form/transaction-form/transaction-form.component';
import { UserFormComponent } from './form/user-form/user-form.component';
import { routing } from './app.routing';
import { ScrollTrackerDirective } from './directive/scrollTracker.directive';
import { UpperCaseTextDirective } from './directive/upperCaseText.directive';
import { OnlyNumberDirective } from './directive/onlyNumber.directive';
import { CurrencyFormatterDirective } from './directive/currencyFormatter.directive';
import { WelcomeMessageComponent } from './welcome-message/welcome-message.component';
import { ItemErrorComponent } from './item-error/item-error.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ItemListComponent } from './leftlist/item-list/item-list.component';
import { AdminListComponent } from './leftlist/admin-list/admin-list.component';
import { CategoryListComponent } from './leftlist/category-list/category-list.component';
import { TransactionListComponent } from './leftlist/transaction-list/transaction-list.component';
import { ReportListComponent } from './leftlist/report-list/report-list.component';
import { UserListComponent } from './leftlist/user-list/user-list.component';
import { AuthGuard } from './authentication/auth-guard.service';
import { DeleteDialogComponent } from './dialog/delete-dialog/delete-dialog.component';
import { LoginDialogComponent } from './dialog/login-dialog/login-dialog.component';
import { LogoutDialogComponent } from './dialog/logout-dialog/logout-dialog.component';
import { ItemFilterDialogComponent } from './dialog/item-filter-dialog/item-filter-dialog.component';
import { TransactionFilterDialogComponent } from './dialog/transaction-filter-dialog/transaction-filter-dialog.component';
import { ErrorDialogComponent } from './dialog/error-dialog/error-dialog.component';
import { SoldDetailDialogComponent } from './dialog/sold-detail-dialog/sold-detail-dialog.component';
import { SoldItemDialogComponent } from './dialog/sold-item-dialog/sold-item-dialog.component';
import { ItemDetailDialogComponent } from './dialog/item-detail-dialog/item-detail-dialog.component';
import { ItemViewDialogComponent } from './dialog/item-view-dialog/item-view-dialog.component';
import { ResetPasswordDialogComponent } from './dialog/reset-password-dialog/reset-password-dialog.component';
import { NewPasswordDialogComponent } from './dialog/new-password-dialog/new-password-dialog.component';
import { ChangePasswordDialogComponent } from './dialog/change-password-dialog/change-password-dialog.component';
import { ItemReportComponent } from './report/item-report/item-report.component';
import { TransactionReportComponent } from './report/transaction-report/transaction-report.component';
import { RouterModule } from "@angular/router";
import { ExcelService } from './service/excel.service';
import { BulkCardComponent } from './card/bulk-card/bulk-card.component';
import { BulkListComponent } from './leftlist/bulk-list/bulk-list.component';
import { BulkItemComponent } from './bulk/item/bulk-item.component';
import { ImportResultDialogComponent } from './dialog/import-result-dialog/import-result-dialog.component';

@NgModule({
  imports: [
      Angular2FontawesomeModule,
      BrowserModule,
      BrowserAnimationsModule,
      ReactiveFormsModule,
      FormsModule,
      HttpModule,
      routing,
      MatInputModule,
      MatTooltipModule,
      MatSelectModule,
      MatCardModule,
      MatButtonModule,
      MatIconModule,
      MatTabsModule,
      MatChipsModule,
      MatDialogModule,
      MatSnackBarModule,
      MatMenuModule,
      MatCheckboxModule,
      MatRadioModule,
      MatTableModule,
      MatPaginatorModule,
      CdkTableModule,
      InfiniteScrollModule
  ],
  declarations: [
      AppComponent,
      DeleteDialogComponent,
      ItemFilterDialogComponent,
      LoginDialogComponent,
      LogoutDialogComponent,
      ErrorDialogComponent,
      ItemDetailDialogComponent,
      SoldDetailDialogComponent,
      SoldItemDialogComponent,
      NewPasswordDialogComponent,
      ResetPasswordDialogComponent,
      ChangePasswordDialogComponent,
      TransactionFilterDialogComponent,
      ItemViewDialogComponent,
      ImportResultDialogComponent,
      UserListComponent,
      UserCardComponent,
      ItemCardComponent,
      UserFormComponent,
      ItemFormComponent,
      CategoryFormComponent,
      TransactionFormComponent,
      WelcomeMessageComponent,
      ItemErrorComponent,
      SidebarComponent,
      ScrollTrackerDirective,
      UpperCaseTextDirective,
      OnlyNumberDirective,
      CurrencyFormatterDirective,
      ItemListComponent,
      AdminListComponent,
      CategoryListComponent,
      TransactionListComponent,
      ReportListComponent,
      CategoryCardComponent,
      AdminCardComponent,
      TransactionCardComponent,
      ReportCardComponent,
      ItemReportComponent,
      TransactionReportComponent,
      BulkCardComponent,
      BulkListComponent,
      BulkItemComponent
  ],
  entryComponents: [
      DeleteDialogComponent,
      ItemFilterDialogComponent,
      LoginDialogComponent,
      LogoutDialogComponent,
      ErrorDialogComponent,
      SoldDetailDialogComponent,
      SoldItemDialogComponent,
      ItemDetailDialogComponent,
      NewPasswordDialogComponent,
      ResetPasswordDialogComponent,
      ChangePasswordDialogComponent,
      TransactionFilterDialogComponent,
      ItemViewDialogComponent,
      ImportResultDialogComponent
  ],
  providers: [
      ItemService,
      TransactionService,
      SharedService,
      UserService,
      DatePipe,
      CurrencyPipe,
      DecimalPipe,
      AuthGuard,
      CategoryService,
      SessionService,
      PrivilegeService,
      ExcelService,
      { provide: UtilityToken, useValue: UtilityList }
  ],
  bootstrap: [AppComponent],
  exports: [
    RouterModule
  ]
})
export class AppModule { }
