import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-welcome-message',
  templateUrl: './welcome-message.component.html',
  styleUrls: ['./welcome-message.component.css']
})
export class WelcomeMessageComponent {

  admin = false;
  category = false;
  item = false;
  transaction = false;
  user = false;
  report = false;
  bulk = false;

  constructor(
      private activatedRoute: ActivatedRoute,
      private router: Router,
  )
  {
    if (this.router.url.indexOf('admin/dashboard') >= 0)
    {
      this.admin = true;
    }else if (this.router.url.indexOf('category/dashboard') >= 0)
    {
      this.category = true;
    }else if (this.router.url.indexOf('transaction/dashboard') >= 0)
    {
      this.transaction = true;
    }else if (this.router.url.indexOf('user/dashboard') >= 0)
    {
      this.user = true;
    }else if (this.router.url.indexOf('report/dashboard') >= 0)
    {
      this.report = true;
    }else if (this.router.url.indexOf('bulk/dashboard') >= 0)
    {
      this.bulk = true;
    }else
    {
      this.item = true;
    }
  }

}
