import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-item-not-found',
  templateUrl: './item-error.component.html',
  styleUrls: ['./item-error.component.css']
})
export class ItemErrorComponent implements OnInit {

  errorTitle;
  errorMessage;
  errorHint;

  constructor(private activatedRoute: ActivatedRoute)
  {
    this.activatedRoute.params.subscribe(params => {
      switch(params['error_code'])
      {
        case '404':
          this.errorTitle = '404 Not Found';
          this.errorMessage = `Halaman tidak ditemukan`;
          this.errorHint = `Coba pilih salah satu dari list di samping kiri`;
          break;
        case '504':
          this.errorTitle = '504 Gateway Timeout';
          this.errorMessage = `Halaman tidak merespon`;
          this.errorHint = `Coba cek jaringan internetmu`;
          break;
      }
    });
  }

  ngOnInit() {
  }

}
