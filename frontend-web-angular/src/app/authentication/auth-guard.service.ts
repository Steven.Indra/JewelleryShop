import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { SessionService } from 'app/service/session.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(private router: Router,
                private sessionService: SessionService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (state.url.indexOf("/home/item/edit/")>=0)
        {
            return true;
        }else if (state.url.indexOf("/home/item/add")>=0)
        {
            return this.checkLogin("Add Item");
        }else if (state.url.indexOf("/admin/category/add")>=0)
        {
            return this.checkLogin("Add Category");
        }else if (state.url.indexOf("/admin/category/edit/")>=0)
        {
            return this.checkLogin("Category Dashboard");
        }else if (state.url.indexOf("/admin/transaction/add")>=0)
        {
            return this.checkLogin("Add Transaction");
        }else if (state.url.indexOf("/admin/transaction/edit/")>=0)
        {
            return this.checkLogin("Transaction Dashboard");
        }else if (state.url.indexOf("/admin/user/add")>=0)
        {
            return this.checkLogin("CUD User");
        }else if (state.url.indexOf("/admin/user/edit/")>=0)
        {
            return this.checkLogin("CUD User");
        }else if (state.url.indexOf("/admin/category/dashboard")>=0)
        {
            return this.checkLogin("Category Dashboard");
        }else if (state.url.indexOf("/admin/dashboard")>=0)
        {
            return this.checkLogin("Admin Dashboard");
        }else if (state.url.indexOf("/admin/transaction/dashboard")>=0)
        {
            return this.checkLogin("Transaction Dashboard");
        }else if (state.url.indexOf("/admin/user/dashboard")>=0)
        {
            return this.checkLogin("User Dashboard");
        }else if (state.url.indexOf("/admin/report/dashboard")>=0)
        {
            return this.checkLogin("Report Dashboard");
        }else if (state.url.indexOf("/admin/report/stockitem")>=0)
        {
            return this.checkLogin("View Report");
        }else if (state.url.indexOf("/admin/report/incomingitem")>=0)
        {
            return this.checkLogin("View Report");
        }else if (state.url.indexOf("/admin/report/exititem")>=0)
        {
            return this.checkLogin("View Report");
        }else if (state.url.indexOf("/admin/report/transaction")>=0)
        {
            return this.checkLogin("View Report");
        }else if (state.url.indexOf("/admin/bulk/item")>=0)
        {
            return this.checkLogin("Bulk Item");
        }else if (state.url.indexOf("/admin/bulk/dashboard")>=0)
        {
            return this.checkLogin("Bulk Dashboard");
        }
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }
    
    checkLogin(name): boolean {
        if (this.sessionService.checkPrivilege(name))
        {
            return true;
        }else
        {
            this.router.navigate(["/"]);
            return false;
        }
    }     
}