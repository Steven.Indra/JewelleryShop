import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-item-filter-dialog',
  templateUrl: './item-filter-dialog.component.html',
  styleUrls: ['./../dialog.css','./item-filter-dialog.component.css']
})
export class ItemFilterDialogComponent{

  itemEntrustedStatus;
  checkedEntrusted = false;
  itemSoldStatus;
  checkedSold = false;
  checkedCategory = false;
  itemCategory;
  categories;
  filterQuery;
  soldStatusOption;
  messageSoldStatus;
  messageItemCategory;

  constructor(
      public dialogRef: MatDialogRef<ItemFilterDialogComponent>
  )
  {
  }

  initiateFilter()
  {
    let success = true;
    let soldValue = false;
    let entrustedValue = false;
    let categoryValue = false;
    if (this.checkedSold)
    {
      if (this.itemSoldStatus)
      {
        soldValue = this.itemSoldStatus;
        this.messageSoldStatus = null;
      }else
      {
        success = false;
        this.messageSoldStatus = "Status Barang tidak boleh kosong";
      }
    }else
    {
      this.messageSoldStatus = null;
    }
    if (this.checkedEntrusted)
    {
      if (this.itemEntrustedStatus == '')
      {
        entrustedValue =  null;
      }else
      {
        entrustedValue = this.itemEntrustedStatus;
      }
    }
    if (this.checkedCategory)
    {
      if (this.itemCategory)
      {
        categoryValue = this.itemCategory;
        this.messageItemCategory = null;
      }else
      {
        success = false;
        this.messageItemCategory = "Kategori Barang tidak boleh kosong";
      }
    }else
    {
      this.messageItemCategory = null;
    }
    if (success)
    {
      this.filterQuery = {
        "itemCategory": categoryValue,
        "itemEntrustedStatus": entrustedValue,
        "itemSold": soldValue
      }
      this.dialogRef.close(this.filterQuery);
    }
  }
}