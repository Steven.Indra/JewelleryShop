import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-import-result-dialog',
  templateUrl: './import-result-dialog.component.html',
  styleUrls: ['./../dialog.css']
})
export class ImportResultDialogComponent {
  constructor(public dialogRef: MatDialogRef<ImportResultDialogComponent>) {}
  successList;
  failureList;
}