import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-sold-detail-dialog',
  templateUrl: './sold-detail-dialog.component.html',
  styleUrls: ['./../dialog.css']
})
export class SoldDetailDialogComponent {
  constructor(public dialogRef: MatDialogRef<SoldDetailDialogComponent>) {}
  item;
}