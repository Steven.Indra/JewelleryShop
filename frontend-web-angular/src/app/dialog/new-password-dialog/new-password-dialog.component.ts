import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-new-password-dialog',
  templateUrl: './new-password-dialog.component.html',
  styleUrls: ['./../dialog.css']
})
export class NewPasswordDialogComponent {
  constructor(public dialogRef: MatDialogRef<NewPasswordDialogComponent>) {}
  newPassword;
  userName;
  userShownName;
}