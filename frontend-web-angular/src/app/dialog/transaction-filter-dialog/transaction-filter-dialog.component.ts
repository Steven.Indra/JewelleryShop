import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-transaction-filter-dialog',
  templateUrl: './transaction-filter-dialog.component.html',
  styleUrls: ['./../dialog.css','./transaction-filter-dialog.component.css']
})
export class TransactionFilterDialogComponent{

  itemEntrustedStatus;
  checkedEntrusted = false;
  itemPIC;
  checkedPIC = false;
  filterQuery;

  constructor(
      public dialogRef: MatDialogRef<TransactionFilterDialogComponent>
  )
  {
  }

  initiateFilter()
  {
    let entrustedValue = false;
    let PICValue = false;
    if (this.checkedEntrusted)
    {
      if (this.itemEntrustedStatus == '')
      {
        entrustedValue =  null;
      }else
      {
        entrustedValue = this.itemEntrustedStatus;
      }
    }
    if (this.checkedPIC)
    {
      if (this.itemPIC == '')
      {
        PICValue =  null;
      }else
      {
        PICValue = this.itemPIC;
      }
    }
    
    this.filterQuery = {
      "itemPIC": PICValue,
      "itemEntrustedStatus": entrustedValue,
    }
    this.dialogRef.close(this.filterQuery);

  }
}