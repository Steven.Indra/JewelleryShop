import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { ItemService } from './../../service/item.service';
import { DatePipe, DecimalPipe } from '@angular/common';


@Component({
  selector: 'app-item-view-dialog',
  templateUrl: './item-view-dialog.component.html',
  styleUrls: ['./../dialog.css','./item-view-dialog.component.css']
})
export class ItemViewDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ItemViewDialogComponent>,
    private itemService: ItemService,
    private datePipe: DatePipe,
    private decimalPipe: DecimalPipe
  ) {}
  selectedItem = {
    itemId  : null,
    itemName : null,
    itemWeight : null,
    itemGoldContent: null,
    itemBuyPrice: null,
    itemBuyDate: null,
    itemPhoto: null,
    itemCategory: {
      categoryId: null,
      categoryName: null
    },
    itemTransaction:null,
    itemEntrustedStatus:null,
    itemNote:null
  };
  selectedItemId;
  item;
  itemNotSold;
  status;
  checkedEntrusted;

  itemPicture = './assets/user-icon.svg';

  ngOnInit() {
    this.selectedItemId = this.item.itemId;
    this.selectedItem = this.item;
    this.changeCheckedEntrusted(this.selectedItem.itemEntrustedStatus);
    this.setItemPicture(undefined);
  }
  
  setItemPicture(optionalLink)
  {
    if (optionalLink)
      this.itemPicture = optionalLink;
    else if (this.selectedItem.itemPhoto)
      this.itemPicture = `//localhost:8080/api/getImage/${ this.selectedItem.itemPhoto }`;
    else
      this.itemPicture = './assets/user-icon.svg';
  }

  changeCheckedEntrusted(value)
  {
    if (value == '' || value == null)
    {
      this.checkedEntrusted = false;
    }else
    {
      this.checkedEntrusted = true;        
    }
  }
}