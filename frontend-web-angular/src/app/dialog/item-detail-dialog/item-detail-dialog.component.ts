import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { ItemService } from './../../service/item.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-item-detail-dialog',
  templateUrl: './item-detail-dialog.component.html',
  styleUrls: ['./../dialog.css','./item-detail-dialog.component.css']
})
export class ItemDetailDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ItemDetailDialogComponent>,
    private itemService: ItemService,
    private datePipe: DatePipe
  ) {}
  selectedItem = {
    itemId  : null,
    itemName : null,
    itemWeight : null,
    itemGoldContent: null,
    itemBuyPrice: null,
    itemBuyDate: null,
    itemPhoto: null,
    itemCategory: {
      categoryId: null,
      categoryName: null
    },
    itemTransaction:null,
    itemEntrustedStatus:null,
    itemNote:null
  };
  selectedItemId;
  item;
  itemNotSold;
  status;
  checkedEntrusted;

  itemPicture = './assets/user-icon.svg';

  ngOnInit() {
    if (this.status == "Change")
    {
      this.getItemsNotSold();
    }else
    {
      this.selectedItemId = this.item.itemId;
      this.selectedItem = this.item;
      this.selectedItem.itemBuyDate = this.convertDate(this.selectedItem.itemBuyDate);
      this.changeCheckedEntrusted(this.selectedItem.itemEntrustedStatus);
    }
    this.setItemPicture(undefined);
  }

  convertDate(milliseconds)
  {
    return this.datePipe.transform(milliseconds, 'yyyy-MM-dd');
  }

  getItemsNotSold()
  {
    this.itemService.getItemsNotSold().subscribe(itemsNotSold => {
      this.itemNotSold = itemsNotSold;
    });
  }

  sendData()
  {
    if (this.selectedItemId)
    {
      return this.dialogRef.close({itemId:this.selectedItemId,item:this.selectedItem});
    }
  }

  setItemPicture(optionalLink)
  {
    if (optionalLink)
      this.itemPicture = optionalLink;
    else if (this.selectedItem.itemPhoto)
      this.itemPicture = `//localhost:8080/api/getImage/${ this.selectedItem.itemPhoto }`;
    else
      this.itemPicture = './assets/user-icon.svg';
  }

  changeValue()
  {
    this.itemService.getItemInfo(this.selectedItemId).subscribe(item => {
      this.selectedItemId = item.itemId;
      this.selectedItem = item;
      this.selectedItem.itemBuyDate = this.convertDate(this.selectedItem.itemBuyDate);
      this.changeCheckedEntrusted(this.selectedItem.itemEntrustedStatus);
      this.setItemPicture(undefined);
    });
  }

  changeCheckedEntrusted(value)
  {
    if (value == '' || value == null)
    {
      this.checkedEntrusted = false;
    }else
    {
      this.checkedEntrusted = true;        
    }
  }
}