import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { UserService } from './../../service/user.service';

@Component({
  selector: 'app-change-password-dialog',
  templateUrl: './change-password-dialog.component.html',
  styleUrls: ['./../dialog.css','./change-password-dialog.component.css']
})
export class ChangePasswordDialogComponent {
  oldPassword;
  newPassword;
  confirmPassword;
  userLoggedData;
  message;
  constructor(public dialogRef: MatDialogRef<ChangePasswordDialogComponent>,
              private userService: UserService) {}

  checkChangePassword()
  {
    let formData : any = new FormData();
    formData.append("userId", this.userLoggedData.userId);
    formData.append("userNewPassword", this.newPassword);
    this.userService.changePassword(formData).subscribe(response => {
      if (response)
      {
        this.userLoggedData.userPassword = this.newPassword;
        return this.dialogRef.close({userLoggedData: this.userLoggedData});
      }else
      {
        return this.dialogRef.close(false);
      }
    });
  }

  tryChangePassword()
  {
    if (this.oldPassword == this.userLoggedData.userPassword)
    {
      if (this.newPassword != "" && this.newPassword != undefined)
      {
        if (this.newPassword == this.confirmPassword)
        {
          this.checkChangePassword();
        }else
        {
          this.message = "Password baru dan konfimasi tidak sama";
        }
      }else
      {
        this.message = "Password baru tidak boleh kosong";
      }
    }else
    {
      this.message = "Password lama salah";
    }
  }
}