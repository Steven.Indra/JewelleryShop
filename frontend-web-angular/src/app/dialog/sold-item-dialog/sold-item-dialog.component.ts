import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-sold-item-dialog',
  templateUrl: './sold-item-dialog.component.html',
  styleUrls: ['./../dialog.css']
})
export class SoldItemDialogComponent {
  constructor(public dialogRef: MatDialogRef<SoldItemDialogComponent>) {}
  item;
  PIC='';
  soldDate='';
  soldPrice='';

  sendData()
  {

    if ((this.PIC != '' && this.PIC != null) && (this.soldDate != '' && this.soldDate != null) && (this.soldPrice != '' && this.soldPrice != null))
    {
      return this.dialogRef.close({itemPIC:this.PIC,itemSoldPrice:this.soldPrice,itemSoldDate:this.soldDate,selectedItem:this.item});
    }
  }
}