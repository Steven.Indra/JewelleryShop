import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-reset-password-dialog',
  templateUrl: './reset-password-dialog.component.html',
  styleUrls: ['./../dialog.css']
})
export class ResetPasswordDialogComponent {
  constructor(public dialogRef: MatDialogRef<ResetPasswordDialogComponent>) {}
  userShownName;
  userName;
}