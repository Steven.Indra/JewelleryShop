import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { UserService } from './../../service/user.service';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./../dialog.css','./login-dialog.component.css']
})
export class LoginDialogComponent {
  username;
  password;
  message;
  constructor(public dialogRef: MatDialogRef<LoginDialogComponent>,
              private userService: UserService) {}

  checkLogin()
  {
    this.userService.tryLogin(this.username).subscribe(response => {
      if (response != "wrong username")
      {
        if (this.password == response.userPassword)
        {
          return this.dialogRef.close({userLoggedIn: response});
        }else
        {
          this.message = "Password salah";
        }
      }else
      {
        this.message = "Username tidak ditemukan";
      }
    });
  }

  tryLogin()
  {
    if (this.username)
    {
      this.checkLogin();
    }else
    {
      this.message = "Username tidak boleh kosong";
    }
  }
}