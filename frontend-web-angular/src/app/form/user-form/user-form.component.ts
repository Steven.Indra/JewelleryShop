import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogConfig, MatDialogRef, MatSnackBar, MatDialog } from '@angular/material';
import { UserService } from './../../service/user.service';
import { PrivilegeService } from './../../service/privilege.service';
import { SharedService } from './../../service/shared-service.service';
import { UtilityToken } from './../../providers';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { ResetPasswordDialogComponent } from './../../dialog/reset-password-dialog/reset-password-dialog.component';
import { NewPasswordDialogComponent } from './../../dialog/new-password-dialog/new-password-dialog.component';
import { ErrorDialogComponent } from './../../dialog/error-dialog/error-dialog.component';

import * as moment from 'moment';
import 'date-input-polyfill';
import { Response } from '@angular/http/src/static_response';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  form;
  errorMessages;
  submitted = false;
  userId;
  selectedUser = {
    userId  : null,
    userName : null,
    userPassword : null,
    userShownName : null,
    privileges: null
  };
  privileges;
  listPrivilege =[];
  isEdit = true;
  userPrivileges= new FormControl();
  private subscription: Subscription;

  constructor(
      private userService: UserService,
      private sharedService: SharedService,
      private privilegeService: PrivilegeService,
      private formBuilder: FormBuilder,
      private activatedRoute: ActivatedRoute,
      public snackBar: MatSnackBar,
      private router: Router,
      private datePipe: DatePipe,
      public dialog: MatDialog,
  )
  {
    this.getAllNewUserPrivilege();
    if (this.router.url.indexOf('edit') >= 0)
    {
      this.activatedRoute.params.subscribe(params => {
        this.userId = params['user_id'];
        this.initializeForm();
        this.isEdit = true;
      });
    }else
    {
      this.isEdit = false;
    }
  }

  checkChecked()
  {
    let arrPriv:Array<String> = [];
    this.listPrivilege.forEach(privilege => {
      if (this.selectedUser.privileges)
      {
        if (this.selectedUser.privileges.filter((e)=>e.privilegeId === privilege.privilegeId).length === 1)
        {
          arrPriv.push(privilege.privilegeId);
        }
      }
    });
    this.userPrivileges.setValue(arrPriv);
  }

  initializeForm()
  {
    this.userService.getUserInfo(this.userId).subscribe(user => {
      this.selectedUser = user;
      this.checkChecked();
      this.sharedService.notifyOther({ option: 'highlight', value: user });
    },
    error =>  {
      this.handleError(error);
    });
  }

  getAllNewUserPrivilege()
  {
    this.privilegeService.getAllNewUserPrivilege().subscribe(privilege => {
      this.listPrivilege = privilege;
    });
  }

  handleError(error)
  {
    if (error.includes('404'))
      this.router.navigate(['error/404']);
    else if (error.includes('504'))
    {
      this.router.navigate(['error/504']);
    }
    else
    {
      let errorTitle = `Generic Error`;
      let errorMessage = `Ada yang salah dengan requestmu. Kontak admin sistem dan beritahu error ini: ${error}`;
      this.openErrorDialog(errorTitle, errorMessage);
    }
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      userName      : new FormControl({value: ''}, Validators.required),
      userPassword  : new FormControl({value: '', disabled: true}),
      userShownName : new FormControl({value: ''}, Validators.required),
    });
  }

  buildPrivilege()
  {
    const arr = this.listPrivilege.map(privilege => {
      return this.formBuilder.control(privilege.selected);
    });
    return this.formBuilder.array(arr);
  }

  convertPassword()
  {
    if (this.selectedUser.userPassword)
    {
      let hiddenPassword = ''
      let passLength = this.selectedUser.userPassword.length;
      for (let i = 0; i <passLength;i++)
      {
        hiddenPassword +='*';
      }
      return hiddenPassword;
    }else
    {
      return '';
    }
  }

  scrollFormToTop()
  {
      const formContainer : any = document.getElementsByClassName('outer-container')[0];
      formContainer.scrollTop = 0;
  }

  sendCancel()
  {
    this.router.navigate(['/admin/user/dashboard']);
    this.sharedService.notifyOther({ option: 'unhighlight', value: '' });
  }

  onSubmit(submittedFormData)
  {
    this.submitted = true;

    const formData : any = new FormData(document.getElementsByTagName('form')[0]);
    formData.append("userPrivileges",this.userPrivileges.value);
    if (this.router.url.indexOf('edit') >= 0)
    {
      formData.append("userPassword",this.selectedUser.userPassword);
      this.userService.edit(formData, this.userId).subscribe((response) => {
        this.sharedService.notifyOther({ option: 'refreshUser', value: 'from form' });
        this.scrollFormToTop();
        this.snackBar.open(`User ${response.userShownName} berhasil diubah!`, 'OK', {
          duration: 1500
        });
      },
      error =>  {
        this.handleError(error);
      });
    }
    else
    {
      formData.append("userPassword",submittedFormData.userName);
      this.userService.add(formData).subscribe((response) => {
        this.sharedService.notifyOther({ option: 'refreshUser', value: 'from form' });
        this.router.navigate(['admin/user/edit',response.userId]);
        this.scrollFormToTop();
        this.snackBar.open(`User ${response.userShownName} berhasil dibuat!`, 'OK', {
          duration: 1500
        });
      },
      error =>  {
        this.handleError(error);
      });
    }
  }

  generateNewPassword() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 6; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
  }

  openResetPasswordDialog()
  {
    let config = new MatDialogConfig();
    let dialogRef = this.dialog.open(ResetPasswordDialogComponent, config);

    dialogRef.componentInstance.userName = this.selectedUser.userName;
    dialogRef.componentInstance.userShownName =  this.selectedUser.userShownName;
    dialogRef.afterClosed().subscribe(result => {
      if (result)
      {
        let formDataChangePassword : any = new FormData();
        formDataChangePassword.append("userId",this.selectedUser.userId);
        formDataChangePassword.append("userNewPassword", this.generateNewPassword());
        this.userService.changePassword(formDataChangePassword).subscribe((response) => {
          this.initializeForm();
          this.scrollFormToTop();
          this.openNewPasswordDialog(response.userPassword);
        },
        error =>  {
          this.handleError(error);
        });
      }
    });
  }

  openNewPasswordDialog(newPassword)
  {
    let config = new MatDialogConfig();
    let dialogRef = this.dialog.open(NewPasswordDialogComponent, config);

    dialogRef.componentInstance.userName = this.selectedUser.userName;
    dialogRef.componentInstance.userShownName =  this.selectedUser.userShownName;
    dialogRef.componentInstance.newPassword =  newPassword;
  }

  openErrorDialog(errorTitle, errorMessage)
  {
    let config = new MatDialogConfig();
    let dialogRef = this.dialog.open(ErrorDialogComponent, config);

    dialogRef.componentInstance.errorTitle = errorTitle;
    dialogRef.componentInstance.errorMessage = errorMessage;
  }
}
