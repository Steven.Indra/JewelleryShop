import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogConfig, MatDialogRef, MatSnackBar, MatDialog, MatCheckbox } from '@angular/material';
import { ItemService } from './../../service/item.service';
import { TransactionService } from './../../service/transaction.service';
import { SharedService } from './../../service/shared-service.service';
import { UtilityToken } from './../../providers';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { ErrorDialogComponent } from './../../dialog/error-dialog/error-dialog.component';
import { ItemDetailDialogComponent } from './../../dialog/item-detail-dialog/item-detail-dialog.component';
import * as moment from 'moment';
import 'date-input-polyfill';
import { SessionService } from 'app/service/session.service';

@Component({
  selector: 'app-transaction-form',
  templateUrl: './transaction-form.component.html',
  styleUrls: ['./transaction-form.component.css']
})
export class TransactionFormComponent implements OnInit {
  form;
  errorMessages;
  submitted = false;
  transactionId;
  selectedTransaction = {
    transactionId  : null,
    transactionDate : null,
    transactionPIC : null,
    itemId: null,
    itemSoldPrice: null,
    item: null,
  };

  isEdit = true;

  privileges = {
    editTransaction:false
  };

  private subscription: Subscription;

  constructor(
      private itemService: ItemService,
      private transactionService: TransactionService,
      private sharedService: SharedService,
      private formBuilder: FormBuilder,
      private activatedRoute: ActivatedRoute,
      public snackBar: MatSnackBar,
      private router: Router,
      private datePipe: DatePipe,
      public dialog: MatDialog,
      private sessionService: SessionService,
      @Inject(UtilityToken) public utilityList
  )
  {
    this.checkPrivilege();

    if (this.router.url.indexOf('edit') >= 0)
    {
      this.activatedRoute.params.subscribe(params => {
        this.transactionId = params['transaction_id'];
        this.initializeForm();
        this.isEdit = true;
      });
    }else
    {
      this.isEdit = false;
    }
  }

  checkPrivilege()
  {
    this.privileges.editTransaction = this.sessionService.checkPrivilege("Edit Transaction");
  }

  initializeForm()
  {
    this.transactionService.getTransactionInfo(this.transactionId).subscribe(transaction => {
      this.selectedTransaction = transaction;
      this.selectedTransaction.transactionDate = this.convertDate(this.selectedTransaction.transactionDate);

      this.sharedService.notifyOther({ option: 'highlight', value: transaction });
    },
    error =>  {
      this.handleError(error);
    });
  }

  handleError(error)
  {
    if (error.includes('404'))
      this.router.navigate(['error/404']);
    else if (error.includes('504'))
    {
      this.router.navigate(['error/504']);
    }
    else if (error.includes('415'))
    {
      let errorTitle = `Unsupported Media Type`;
      let errorMessage = `Hanya menerima JPG dan PNG untuk gambar profile.`;
      this.openErrorDialog(errorTitle, errorMessage);
    }
    else
    {
      let errorTitle = `Generic Error`;
      let errorMessage = `Ada yang salah dengan requestmu. Kontak admin sistem dan beritahu error ini:
      ${error}`;
      this.openErrorDialog(errorTitle, errorMessage);
    }
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      transactionId   : new FormControl({value: '', disabled: true}),
      transactionDate : new FormControl({value: '', disabled: this.isEdit}, [this.validateDate,Validators.required]),
      transactionPIC  : new FormControl({value: '', disabled: this.checkFormDisabled()}, Validators.required),
      itemId          : new FormControl({value: '', disabled: true}, Validators.required),
      itemSoldPrice   : new FormControl({value: '', disabled: this.checkFormDisabled()}, Validators.required)
    });
  }

  checkFormDisabled()
  {
    return this.isEdit && !this.privileges.editTransaction;
  }

  scrollFormToTop()
  {
        const formContainer : any = document.getElementsByClassName('outer-container')[0];
        formContainer.scrollTop = 0;
  }

  sendCancel()
  {
    this.router.navigate(['/admin/transaction/dashboard']);
    this.sharedService.notifyOther({ option: 'unhighlight', value: '' });
  }

  convertDate(milliseconds)
  {
    return this.datePipe.transform(milliseconds, 'yyyy-MM-dd');
  }

  validateDate(date)
  {
    if (moment(date.value, ['MM/DD/YYYY', 'M/D/YYYY', 'YYYY-MM-DD'], true).isValid() || !date.value)
      return null;
    else
    {
      return {
        validateDate : {
          valid : false
        }
      };
    }
  }

  onSubmit(submittedFormData)
  {
    this.submitted = true;
    if (this.selectedTransaction.itemId)
    {
      //Required to trigger invalid style on md-select elements.
      const formData : any = new FormData();
      console.log(submittedFormData);
      //Add data from select options
      formData.append("itemPIC", this.selectedTransaction.transactionPIC);
      formData.append("itemSoldDate", this.selectedTransaction.transactionDate);
      formData.append("itemSoldPrice", this.selectedTransaction.itemSoldPrice.toString().replace(new RegExp("[,]", "g"), ""));
      formData.append("itemId", this.selectedTransaction.itemId);

      if (this.router.url.indexOf('edit') >= 0)
      {
        formData.append("transactionId", this.selectedTransaction.transactionId);
        this.transactionService.edit(formData).subscribe((response) => {
          this.sharedService.notifyOther({ option: 'refresh', value: 'from form' });
          this.initializeForm();
          this.scrollFormToTop();
          this.snackBar.open(`Transaksi ${response.transactionId} berhasil diubah!`, 'OK', {
            duration: 1500
          });
        },
        error =>  {
          this.handleError(error);
        });
      }
      else
      {
        this.transactionService.add(formData).subscribe((response) => {
            this.router.navigate(['/admin/transaction/edit',response.transactionId]);
            this.scrollFormToTop();
            this.snackBar.open(`Transaksi ${response.transactionId} berhasil dibuat!`, 'OK', {
              duration: 1500
            });
            this.sharedService.notifyOther({ option: 'refresh', value: 'from form' });
        },
        error =>  {
          this.handleError(error);
          this.sharedService.notifyOther({ option: 'refresh', value: 'from form' });
        });
      }
    }    
  }

  openErrorDialog(errorTitle, errorMessage)
  {
    let config = new MatDialogConfig();
    let dialogRef = this.dialog.open(ErrorDialogComponent, config);

    dialogRef.componentInstance.errorTitle = errorTitle;
    dialogRef.componentInstance.errorMessage = errorMessage;
  }

  openItemDetailDialog(status)
  {
    let config = new MatDialogConfig();
    let dialogRef = this.dialog.open(ItemDetailDialogComponent, config);
    dialogRef.componentInstance.status = status;
    if (status == 'Info')
    {
      dialogRef.componentInstance.item = this.selectedTransaction.item;  
    }
    dialogRef.afterClosed().subscribe(result => {
      if (result)
      {
          this.selectedTransaction.itemId = result.itemId;
          this.selectedTransaction.item = result.item;
      }
    });
  }
}
