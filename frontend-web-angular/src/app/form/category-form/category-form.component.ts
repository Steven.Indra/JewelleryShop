import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogConfig, MatDialogRef, MatSnackBar, MatDialog } from '@angular/material';
import { CategoryService } from './../../service/category.service';
import { SharedService } from './../../service/shared-service.service';
import { UtilityToken } from './../../providers';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { ErrorDialogComponent } from './../../dialog/error-dialog/error-dialog.component';
import { SessionService } from 'app/service/session.service';

import * as moment from 'moment';
import 'date-input-polyfill';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css']
})
export class CategoryFormComponent implements OnInit {
  form;
  errorMessages;
  submitted = false;
  categoryId;
  selectedCategory = {
    categoryId  : null,
    categoryName : null,
    description : null,
  };

  isEdit = true;
  
  privileges = {
    editCategory:false
  };

  private subscription: Subscription;

  constructor(
      private categoryService: CategoryService,
      private sessionService: SessionService,
      private sharedService: SharedService,
      private formBuilder: FormBuilder,
      private activatedRoute: ActivatedRoute,
      public snackBar: MatSnackBar,
      private router: Router,
      private datePipe: DatePipe,
      public dialog: MatDialog,
  )
  {
    this.checkPrivilege();

    if (this.router.url.indexOf('edit') >= 0)
    {
      this.activatedRoute.params.subscribe(params => {
        this.categoryId = params['category_id'];
        this.initializeForm();
      });
    }
  }

  initializeForm()
  {
    this.categoryService.getCategoryInfo(this.categoryId).subscribe(category => {
      this.selectedCategory = category;
      this.sharedService.notifyOther({ option: 'highlight', value: category });
    },
    error =>  {
      this.handleError(error);
    });
  }

  handleError(error)
  {
    if (error.includes('404'))
      this.router.navigate(['error/404']);
    else if (error.includes('504'))
    {
      this.router.navigate(['error/504']);
    }
    else
    {
      let errorTitle = `Generic Error`;
      let errorMessage = `Ada yang salah dengan requestmu. Kontak admin sistem dan beritahu error ini: ${error}`;
      this.openErrorDialog(errorTitle, errorMessage);
    }
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      categoryName  : new FormControl({value: '', disabled: this.checkFormDisabled()}, Validators.required),
      description   : new FormControl({value: '', disabled: this.checkFormDisabled()})
    });
  }

  checkPrivilege()
  {
    this.privileges.editCategory = this.sessionService.checkPrivilege("Edit Category");
  }

  checkFormDisabled()
  {    
    return this.isEdit && !this.privileges.editCategory;
  }

  scrollFormToTop()
  {
        const formContainer : any = document.getElementsByClassName('outer-container')[0];
        formContainer.scrollTop = 0;
  }

  sendCancel()
  {
    this.router.navigate(['/admin/category/dashboard']);
    this.sharedService.notifyOther({ option: 'unhighlight', value: '' });
  }

  onSubmit(submittedFormData)
  {
    this.submitted = true;

    const formData : any = new FormData(document.getElementsByTagName('form')[0]);

    if (this.router.url.indexOf('edit') >= 0)
    {
      this.categoryService.edit(formData, this.categoryId).subscribe((response) => {
        this.sharedService.notifyOther({ option: 'refreshCategory', value: 'from form' });
        this.scrollFormToTop();
        this.snackBar.open(`Kategori ${response.categoryName} berhasil diubah!`, 'OK', {
          duration: 1500
        });
      },
      error =>  {
        this.handleError(error);
      });
    }
    else
    {
      this.categoryService.add(formData).subscribe((response) => {
        this.sharedService.notifyOther({ option: 'refreshCategory', value: 'from form' });
        this.router.navigate(['admin/category/edit',response.categoryId]);
        this.scrollFormToTop();
        this.snackBar.open(`Kategori ${response.categoryName} berhasil dibuat!`, 'OK', {
          duration: 1500
        });
      },
      error =>  {
        this.handleError(error);
      });
    }
  }

  openErrorDialog(errorTitle, errorMessage)
  {
    let config = new MatDialogConfig();
    let dialogRef = this.dialog.open(ErrorDialogComponent, config);

    dialogRef.componentInstance.errorTitle = errorTitle;
    dialogRef.componentInstance.errorMessage = errorMessage;
  }
}
