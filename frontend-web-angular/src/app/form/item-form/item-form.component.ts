import { Component, OnInit, Inject, isDevMode } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogConfig, MatDialogRef, MatSnackBar, MatDialog, MatCheckbox } from '@angular/material';
import { ItemService } from './../../service/item.service';
import { TransactionService } from './../../service/transaction.service';
import { CategoryService } from './../../service/category.service';
import { SharedService } from './../../service/shared-service.service';
import { UtilityToken } from './../../providers';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { ErrorDialogComponent } from './../../dialog/error-dialog/error-dialog.component';
import { SoldDetailDialogComponent } from './../../dialog/sold-detail-dialog/sold-detail-dialog.component';
import { SoldItemDialogComponent } from './../../dialog/sold-item-dialog/sold-item-dialog.component';
import * as moment from 'moment';
import 'date-input-polyfill';
import { SessionService } from 'app/service/session.service';

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.css']
})
export class ItemFormComponent implements OnInit {
  form;
  categories;
  forbiddenFile;
  errorMessages;
  submitted = false;
  itemId;
  itemPicture = './assets/user-icon.svg';
  selectedItem = {
    itemId  : null,
    itemName : null,
    itemWeight : null,
    itemGoldContent: null,
    itemBuyPrice: null,
    itemBuyDate: null,
    itemPhoto: null,
    itemCategory: {
      categoryId: null,
      categoryName: null
    },
    itemTransaction:null,
    itemEntrustedStatus:null,
    itemNote:null
  };

  private subscription: Subscription;
  userLoggedIn = false;
  checkedEntrusted = false;
  isEdit =false;

  privileges = {
    editItem: false
  };

  constructor(
      private itemService: ItemService,
      private transactionService: TransactionService,
      private categoryService: CategoryService,
      private sharedService: SharedService,
      private formBuilder: FormBuilder,
      private activatedRoute: ActivatedRoute,
      public snackBar: MatSnackBar,
      private router: Router,
      private datePipe: DatePipe,
      public dialog: MatDialog,
      private sessionService: SessionService,
      @Inject(UtilityToken) public utilityList
  )
  {
    this.checkSession();

    this.subscription = this.sharedService.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('option') && res.option === 'loginStatus' && res.value === "login") 
      {
        this.checkSession();
        this.checkPrivilege();
        this.enableForm();
      }else if (res.hasOwnProperty('option') && res.option === 'loginStatus' && res.value === "logout") 
      {
        this.checkSession();
        this.checkPrivilege();
        this.disableForm();
      }
    });

    this.checkPrivilege();

    this.categoryService.getAll().subscribe( categories => {
      this.categories = categories;
    });

    this.checkEdited();
  }

  checkSession()
  {
    if (this.sessionService.retrieveSession())
    {
      this.userLoggedIn = true;
    }else
    {
      this.userLoggedIn = false;
    }
  }

  checkEdited()
  {
    if (this.router.url.indexOf('edit') >= 0)
    {
      this.activatedRoute.params.subscribe(params => {
        this.itemId = params['item_id'];
        this.initializeForm();
        this.isEdit = true;
      });
    }else
    {
      this.isEdit = false;
    }
  }

  checkPrivilege()
  {
    this.privileges.editItem = this.sessionService.checkPrivilege("Edit Item");
  }

  enableForm()
  {
      this.form.get("itemId").enable();
      this.form.get("itemName").enable();
      this.form.get("itemWeight").enable();
      this.form.get("itemGoldContent").enable();
      this.form.get("itemBuyPrice").enable();
      this.form.get("itemBuyDate").enable();
      this.form.get("itemCategoryId").enable();
      this.form.get("itemPhoto").enable();
      this.form.get("itemEntrustedStatus").enable();
      this.form.get("itemNote").enable();
  }

  disableForm()
  {
      this.form.get("itemId").disable();
      this.form.get("itemName").disable();
      this.form.get("itemWeight").disable();
      this.form.get("itemGoldContent").disable();
      this.form.get("itemBuyPrice").disable();
      this.form.get("itemBuyDate").disable();
      this.form.get("itemCategoryId").disable();
      this.form.get("itemPhoto").disable();
      this.form.get("itemEntrustedStatus").disable();
      this.form.get("itemNote").disable();
  }

  initializeForm()
  {
    this.itemService.getItemInfo(this.itemId).subscribe(item => {
      this.selectedItem = item;
      
      if (this.selectedItem.itemEntrustedStatus == '' || this.selectedItem.itemEntrustedStatus == null)
      {
        this.checkedEntrusted = false;
        this.form.get("itemEntrustedStatus").disable();
      }else
      {
        this.checkedEntrusted = true;
        if (this.privileges.editItem)
        {
          this.form.get("itemEntrustedStatus").enable();
        }else
        {
          this.form.get("itemEntrustedStatus").disable();
        }
        
      }

      this.setItemPicture(undefined);
      this.selectedItem.itemBuyDate = this.convertDate(this.selectedItem.itemBuyDate);
      if (this.selectedItem.itemTransaction)
      {
        this.selectedItem.itemTransaction.transactionDate = this.convertDate(this.selectedItem.itemTransaction.transactionDate);
      }
    
      let fileInput = <HTMLInputElement>document.getElementById('itemPhoto');
      fileInput.value = '';

      this.sharedService.notifyOther({ option: 'highlight', value: item });
    },
    error =>  {
      this.handleError(error);
    });
  }

  handleError(error)
  {
    if (error.includes('404'))
      this.router.navigate(['error/404']);
    else if (error.includes('504'))
    {
      this.router.navigate(['error/504']);
    }
    else if (error.includes('415'))
    {
      let errorTitle = `Unsupported Media Type`;
      let errorMessage = `Hanya menerima JPG dan PNG untuk gambar profile`;
      this.openErrorDialog(errorTitle, errorMessage);
    }
    else if (error.includes('409'))
    {
      let errorTitle = `Unique`;
      let errorMessage = `Id Barang sudah digunakan.`;
      this.openErrorDialog(errorTitle, errorMessage);
    }
    else
    {
      let errorTitle = `Generic Error`;
      let errorMessage = `Ada yang salah dengan requestmu. Kontak admin sistem dan beritahu error ini:
      ${error}`;
      this.openErrorDialog(errorTitle, errorMessage);
    }
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      itemId                : new FormControl({value: '', disabled: this.isEdit}, Validators.required),
      itemName              : new FormControl({value: '', disabled: this.checkFormDisabled()}, Validators.required),
      itemWeight            : new FormControl({value: '', disabled: this.checkFormDisabled()}, Validators.required),
      itemGoldContent       : new FormControl({value: '', disabled: this.checkFormDisabled()}, Validators.required),
      itemBuyPrice          : new FormControl({value: '', disabled: this.checkFormDisabled()}, Validators.required),
      itemBuyDate           : new FormControl({value: '', disabled: this.checkFormDisabled()}, [this.validateDate,Validators.required]),
      itemCategoryId        : new FormControl({value: '', disabled: this.checkFormDisabled()}, Validators.required),
      itemPhoto             : new FormControl({value: '', disabled: this.checkFormDisabled()}),
      itemEntrustedStatus   : new FormControl({value: '', disabled: this.checkFormDisabled()||!this.checkedEntrusted}),
      itemNote              : new FormControl({value: '', disabled: this.checkFormDisabled()}),
    });
  }

  checkFormDisabled()
  {    
    return this.isEdit && !this.privileges.editItem;
  }

  scrollFormToTop()
  {
        const formContainer : any = document.getElementsByClassName('outer-container')[0];
        formContainer.scrollTop = 0;
  }

  sendCancel()
  {
    this.router.navigate(['/']);
    this.sharedService.notifyOther({ option: 'unhighlight', value: '' });
  }

  sendSold(itemPIC, itemSoldDate, itemSoldPrice, item)
  {
    const formDataSold : any = new FormData();
    formDataSold.append("itemPIC", itemPIC);
    formDataSold.append("itemSoldDate", itemSoldDate);
    formDataSold.append("itemSoldPrice", itemSoldPrice);
    formDataSold.append("itemId", item.itemId);
    this.transactionService.add(formDataSold).subscribe((response) => {
        this.scrollFormToTop();
        this.snackBar.open(`Barang ${item.itemId}-${item.itemName} telah terjual seharga ${response.itemSoldPrice}!`, 'OK', {
          duration: 1500
        });
        this.initializeForm();
        this.sharedService.notifyOther({ option: 'refresh', value: 'from form' });
    },
    error =>  {
      this.handleError(error);
    });
  }

  changeDisabled()
  {
    if (this.checkedEntrusted)
    {
      this.form.get("itemEntrustedStatus").enable();
    }else
    {
      this.form.get("itemEntrustedStatus").disable();
    }
  }

  convertDate(milliseconds)
  {
    return this.datePipe.transform(milliseconds, 'yyyy-MM-dd');
  }

  validateDate(date)
  {
    if (moment(date.value, ['MM/DD/YYYY', 'M/D/YYYY', 'YYYY-MM-DD'], true).isValid() || !date.value)
      return null;
    else
    {
      return {
        validateDate : {
          valid : false
        }
      };
    }
  }

  setItemPicture(optionalLink)
  {
    if (optionalLink)
      this.itemPicture = optionalLink;
    else if (this.selectedItem.itemPhoto)
      this.itemPicture = `//localhost:8080/api/getImage/${ this.selectedItem.itemPhoto }`;
    else
      this.itemPicture = './assets/user-icon.svg';
  }

  getSoldIcon()
  {
      return `./assets/sold-icon.png`;
  }

  triggerUploadWindow()
  {
    document.getElementById('trigger-photo-upload').click();
  }

  setFile(event)
  {
    const reader = new FileReader();
    const allowedTypes = ['image/jpeg', 'image/png'];
    const file = event.target.files[0];

    this.forbiddenFile = false;

    if (file)
    {
      if ((file.type === allowedTypes[0] || file.type === allowedTypes[1]) && file.size <= 1048576)
      {
        reader.onload = (event: any) =>
        {
          this.setItemPicture(event.target.result);
        };

        reader.readAsDataURL(event.target.files[0]);
      }
      else
      {
        this.forbiddenFile = true;
      }
    }
  }

  onSubmit(submittedFormData)
  {
    this.submitted = true;

    //Required to trigger invalid style on md-select elements.
    if (!this.form.valid || this.forbiddenFile)
    {
      this.form.get('itemCategoryId').markAsTouched();
      return false;
    }
    const formData : any = new FormData(document.getElementsByTagName('form')[0]);

    //Add data from select options
    formData.append('itemCategoryId', submittedFormData.itemCategoryId);
    formData.append('itemBuyPriceFormatted', submittedFormData.itemBuyPrice.toString().replace(new RegExp("[,]", "g"), ""));
    let entrustedValue = "";
    if (submittedFormData.itemEntrustedStatus == undefined || submittedFormData.itemEntrustedStatus == null )
    {
      entrustedValue = "";
    }else
    {
      entrustedValue = submittedFormData.itemEntrustedStatus;
    }
    formData.append('itemEntrustedStatusChanged', entrustedValue);
    
    if (this.router.url.indexOf('edit') >= 0)
    {
      formData.append("itemId", this.selectedItem.itemId);
      this.itemService.edit(formData).subscribe((response) => {
        this.sharedService.notifyOther({ option: 'refresh', value: 'from form' });
        this.initializeForm();
        this.scrollFormToTop();
        this.snackBar.open(`Barang ${response.itemId}-${response.itemName} berhasil diubah!`, 'OK', {
          duration: 1500
        });
      },
      error =>  {
        this.handleError(error);
      });
    }
    else
    {
      this.itemService.add(formData).subscribe((response) => {
          this.router.navigate(['/home/item/edit',response.itemId]);
          this.scrollFormToTop();
          this.snackBar.open(`Barang ${response.itemId}-${response.itemName} berhasil dibuat!`, 'OK', {
            duration: 1500
          });
          this.sharedService.notifyOther({ option: 'refresh', value: 'from form' });
      },
      error =>  {
        this.handleError(error);
        this.sharedService.notifyOther({ option: 'refresh', value: 'from form' });
      });
    }
  }

  openErrorDialog(errorTitle, errorMessage)
  {
    let config = new MatDialogConfig();
    let dialogRef = this.dialog.open(ErrorDialogComponent, config);

    dialogRef.componentInstance.errorTitle = errorTitle;
    dialogRef.componentInstance.errorMessage = errorMessage;
  }

  openSoldDetailDialog(item)
  {
    let config = new MatDialogConfig();
    let dialogRef = this.dialog.open(SoldDetailDialogComponent, config);
    dialogRef.componentInstance.item = item;
  }

  openSoldItemDialog(item)
  {
    let config = new MatDialogConfig();
    let dialogRef = this.dialog.open(SoldItemDialogComponent, config);
    dialogRef.componentInstance.item = item;
    dialogRef.componentInstance.PIC = this.userLoggedIn? this.sessionService.retrieveSession().userShownName:'';
    dialogRef.componentInstance.soldDate = this.datePipe.transform(new Date(),"yyyy-MM-dd");
    dialogRef.afterClosed().subscribe(result => {
      if (result)
      {
          this.sendSold(result.itemPIC, result.itemSoldDate, result.itemSoldPrice.replace(new RegExp("[,]", "g"), ""), result.selectedItem);
      }
    });
  }
}
