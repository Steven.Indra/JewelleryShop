import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-transaction-card',
  templateUrl: './transaction-card.component.html',
  styleUrls: ['./transaction-card.component.css']
})
export class TransactionCardComponent {

  @Input() transaction;
  @Input() selectedId;
}
