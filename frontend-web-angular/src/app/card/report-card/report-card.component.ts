import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-report-card',
  templateUrl: './report-card.component.html',
  styleUrls: ['./report-card.component.css']
})
export class ReportCardComponent {

  @Input() report;
  @Input() selectedReport;
}
