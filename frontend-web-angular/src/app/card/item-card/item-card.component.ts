import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.css']
})
export class ItemCardComponent {

  @Input() item;
  @Input() selectedId;
  photoPath = "./assets";

  getPictureLink()
  {
    if (this.item.itemPhoto)
      return `//localhost:8080/api/getImage/${ this.item.itemPhoto }` ;
    else 
      return `${ this.photoPath }/user-icon.svg`;
  }

  getSoldIcon()
  {
      return `${ this.photoPath }/sold-icon.png`;
  }
}
