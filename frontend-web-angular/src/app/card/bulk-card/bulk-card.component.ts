import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-bulk-card',
  templateUrl: './bulk-card.component.html',
  styleUrls: ['./bulk-card.component.css']
})
export class BulkCardComponent {

  @Input() bulk;
  @Input() selectedBulk;
}
