import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogConfig, MatDialogRef, MatSnackBar, MatDialog, MatCheckbox } from '@angular/material';
import { ItemService } from './../../service/item.service';
import { TransactionService } from './../../service/transaction.service';
import { CategoryService } from './../../service/category.service';
import { SharedService } from './../../service/shared-service.service';
import { UtilityToken } from './../../providers';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { ErrorDialogComponent } from './../../dialog/error-dialog/error-dialog.component';
import * as moment from 'moment';
import 'date-input-polyfill';
import { SessionService } from 'app/service/session.service';
import { ExcelService } from 'app/service/excel.service';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
import { ItemViewDialogComponent } from 'app/dialog/item-view-dialog/item-view-dialog.component';

@Component({
  selector: 'app-transaction-report',
  templateUrl: './transaction-report.component.html',
  styleUrls: ['./transaction-report.component.css']
})
export class TransactionReportComponent implements OnInit {

    
    yearList;
    exportData:ExportData[] = [];
    selectedYear = (new Date()).getFullYear().toString();
    selectedMonth = (new Date()).getMonth() + 1;
    userLoggedIn = false;
    checkedMonth = false;
    reportTitle;
    selectedReport;
    privileges = {
        viewReport: false
    };
    entrustedOption = 0;
    totalItem = 0;
    totalWeight= 0;

    @ViewChild(MatPaginator) paginator : MatPaginator;
    displayedColumns = ['transactionId', 'transactionDate', 'transactionPIC','itemId','itemSoldPrice','viewItemDetail'];
    itemRetrievedData;
    dataSource: ItemDataSource | null;

    ngOnInit() {
        this.requestData();
    }

    requestData()
    {
        let monthValue = -1;
        let yearValue = -1;
        if (this.checkedMonth)
        {   
            monthValue = this.selectedMonth;
        }
        yearValue = +this.selectedYear;

        this.transactionService.getReport(yearValue,monthValue,this.entrustedOption).subscribe(response => {
            this.fetchData(response);
        });
    }

    fetchData(transaction)
    {
        this.convertToExportData(transaction); //for export purpose
        this.totalWeight = 0;
        this.totalItem = transaction.length;
        this.itemRetrievedData = new ItemRetrieve(transaction);
        this.dataSource = new ItemDataSource(this.itemRetrievedData,this.paginator);
    }

    private subscription: Subscription;
    constructor(
        private itemService: ItemService,
        private transactionService: TransactionService,
        private categoryService: CategoryService,
        private sharedService: SharedService,
        private activatedRoute: ActivatedRoute,
        public snackBar: MatSnackBar,
        private router: Router,
        private datePipe: DatePipe,
        public dialog: MatDialog,
        private sessionService: SessionService,
        private excelService: ExcelService,
        @Inject(UtilityToken) public utilityList
    )
    {
        this.checkSession();
        
        this.checkPrivilege();
        
        this.checkPath();

        this.getYearList();
    }

    checkPath()
    {
        if (this.router.url.indexOf('report/transaction') >= 0)
        {
            this.checkSelectedReport("Transaction");
        }
    }

    getYearList()
    {
        this.itemService.getItemYear(this.selectedReport).subscribe(response =>{
            this.yearList = response;          
        });
    }

    checkSelectedReport(reportItem)
    {
        this.selectedReport = reportItem;
        this.reportTitle = {
            'Transaction': 'Transaksi',
        }[this.selectedReport];
    }

    search()
    {
        this.paginator.pageIndex = 0;
        this.requestData();
    }

    viewItemDetail(item)
    {
        this.openItemDetailDialog(item)
    }

    openItemDetailDialog(item)
    {
      let config = new MatDialogConfig();
      let dialogRef = this.dialog.open(ItemViewDialogComponent, config);
      item.itemBuyDate = this.convertDate(item.itemBuyDate);
      dialogRef.componentInstance.item = item;  
      dialogRef.componentInstance.status = "Info";
    }

    checkSession()
    {
        if (this.sessionService.retrieveSession())
        {
        this.userLoggedIn = true;
        }else
        {
        this.userLoggedIn = false;
        }
    }

    checkPrivilege()
    {
        this.privileges.viewReport = this.sessionService.checkPrivilege("View Report");
    }

    convertToExportData(transaction)
    {
        this.exportData = [];
        transaction.forEach(element => {
            let tempExportData={
                transactionId: null,
                transactionDate:null,
                transactionPIC : null,
                itemId: null,
                itemSoldPrice: null,
            };
            tempExportData.transactionId = element.transactionId;
            tempExportData.transactionDate = this.convertDate(element.transactionDate);
            tempExportData.transactionPIC = element.transactionPIC;
            tempExportData.itemId = element.itemId;
            tempExportData.itemSoldPrice = element.itemSoldPrice;
            this.exportData.push(tempExportData);
        });
    }

    sendExport()
    {
        let fileName = this.selectedReport.replace(" ","") + this.selectedYear;
        if (this.checkedMonth)
        {
            fileName += this.pad(this.selectedMonth,2);
        }
        if (this.entrustedOption == 0)
        {
            fileName += "Semua";
        }else if (this.entrustedOption == 1)
        {
            fileName += "BarangOrang";
        }else if (this.entrustedOption == 2)
        {
            fileName += "BarangSendiri";
        }
        fileName += ".csv"
        this.excelService.exportExcel(this.exportData,fileName);
    }

    pad(num:number, size:number): string {
        let s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }

    sendCancel()
    {
        this.router.navigate(['/admin/report/dashboard']);
        this.sharedService.notifyOther({ option: 'unhighlight', value: '' });
    }

    convertDate(milliseconds)
    {
        return this.datePipe.transform(milliseconds, 'yyyy-MM-dd');
    }

    openErrorDialog(errorTitle, errorMessage)
    {
        let config = new MatDialogConfig();
        let dialogRef = this.dialog.open(ErrorDialogComponent, config);

        dialogRef.componentInstance.errorTitle = errorTitle;
        dialogRef.componentInstance.errorMessage = errorMessage;
    }
}

export interface ExportData {
    transactionId: null;
    transactionDate:null;
    transactionPIC : null,
    itemId: null,
    itemSoldPrice: null
}

export interface Item {
    transactionId: null;
    transactionDate:null;
    transactionPIC : null,
    itemId: null,
    itemSoldPrice: null
}

/** An example database that the data source uses to retrieve data for the table. */
export class ItemRetrieve {
/** Stream that emits whenever the data has been modified. */
    dataChange: BehaviorSubject<Item[]> = new BehaviorSubject<Item[]>([]);
    get data(): Item[] { return this.dataChange.value; }

    constructor(private item:Item) {
    // Fill up the database with 100 users.
        this.addItem(item);  
    }

    addItem(item)
    {
        for (let i=0;i<item.length;i++)
        {
            const copiedData = this.data.slice();
            copiedData.push(item[i]);
            this.dataChange.next(copiedData);
        }
    }

    getLength()
    {
        return this.dataChange.value.length;
    }
}

/**
* Data source to provide what data should be rendered in the table. Note that the data source
* can retrieve its data in any way. In this case, the data source is provided a reference
* to a common data base, ExampleDatabase. It is not the data source's responsibility to manage
* the underlying data. Instead, it only needs to take the data and send the table exactly what
* should be rendered.
*/
export class ItemDataSource extends DataSource<any> {
    constructor(private itemRetrievedData: ItemRetrieve, private _paginator: MatPaginator) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<Item[]> {
        const displayDataChanges = [
            this.itemRetrievedData.dataChange,
            this._paginator.page
        ];
        return Observable.merge(...displayDataChanges).map(() => {
            const data = this.itemRetrievedData.data.slice();

            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        })
    }

    disconnect() {}
}
