import { Component, OnInit, Inject, ViewChild, isDevMode } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogConfig, MatDialogRef, MatSnackBar, MatDialog, MatCheckbox } from '@angular/material';
import { ItemService } from './../../service/item.service';
import { TransactionService } from './../../service/transaction.service';
import { CategoryService } from './../../service/category.service';
import { SharedService } from './../../service/shared-service.service';
import { UtilityToken } from './../../providers';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe, DecimalPipe } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { ErrorDialogComponent } from './../../dialog/error-dialog/error-dialog.component';
import * as moment from 'moment';
import 'date-input-polyfill';
import { SessionService } from 'app/service/session.service';
import { ExcelService } from 'app/service/excel.service';
import { MatPaginator } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
import { ItemViewDialogComponent } from './../../dialog/item-view-dialog/item-view-dialog.component';
import { SoldDetailDialogComponent } from './../../dialog/sold-detail-dialog/sold-detail-dialog.component';
import * as Papa from 'papaparse/papaparse.min.js';


@Component({
  selector: 'app-item-report',
  templateUrl: './item-report.component.html',
  styleUrls: ['./item-report.component.css']
})
export class ItemReportComponent implements OnInit {

    yearList;
    exportData:ExportData[] = [];
    selectedYear = (new Date()).getFullYear().toString();
    selectedMonth = (new Date()).getMonth() + 1;
    userLoggedIn = false;
    checkedMonth = false;
    selectedReport;
    reportTitle;
    privileges = {
        viewReport: false
    };
    entrustedOption = 0;
    totalItem = 0;
    totalWeight= 0;
    @ViewChild(MatPaginator) paginator : MatPaginator;
    displayedColumnsStock = ['itemId', 'itemName', 'itemCategory','itemWeight','itemGoldContent','itemBuyDate','itemBuyPrice','itemEntrustedStatus','viewDetail'];
    displayedColumnsIncoming = ['itemId', 'itemName', 'itemCategory','itemWeight','itemGoldContent','itemBuyDate','itemBuyPrice','itemEntrustedStatus','itemTransaction','viewDetail'];
    itemRetrievedData;
    dataSource: ItemDataSource | null;
  
    ngOnInit() {
        this.requestData();
    }

    requestData()
    {
        let monthValue = -1;
        let yearValue = -1;
        if (this.selectedReport != "Stock Item")
        {
            if (this.checkedMonth)
            {   
                monthValue = this.selectedMonth;
            }
            yearValue = +this.selectedYear;
        }

        this.itemService.getReport(this.selectedReport,yearValue,monthValue,this.entrustedOption).subscribe(response => {
            this.fetchData(response);
        });
        
    }

    fetchData(item)
    {
        this.convertToExportData(item); //for export purpose
        this.totalWeight = 0;
        this.totalItem = item.length;
        item.forEach(element => {
            this.totalWeight = Math.round(this.totalWeight * 10000) / 10000  + Math.round(element.itemWeight * 10000) / 10000;
        });
        this.itemRetrievedData = new ItemRetrieve(item);
        this.dataSource = new ItemDataSource(this.itemRetrievedData,this.paginator);
    }

    private subscription: Subscription;
    constructor(
        private itemService: ItemService,
        private transactionService: TransactionService,
        private categoryService: CategoryService,
        private sharedService: SharedService,
        private activatedRoute: ActivatedRoute,
        public snackBar: MatSnackBar,
        private router: Router,
        private datePipe: DatePipe,
        public dialog: MatDialog,
        private sessionService: SessionService,
        private excelService: ExcelService,
        @Inject(UtilityToken) public utilityList
    )
    {
        this.checkSession();
        
        this.checkPrivilege();
        
        this.checkPath();

        this.getYearList();
        
    }

    viewItemDetail(item)
    {
        this.openItemDetailDialog(item)
    }

    search()
    {
        this.paginator.pageIndex = 0;
        this.requestData();
    }

    getYearList()
    {
        this.itemService.getItemYear(this.selectedReport).subscribe(response =>{
            this.yearList = response;          
        });
    }

    convertDate(milliseconds)
    {
      return this.datePipe.transform(milliseconds, 'yyyy-MM-dd');
    }

    checkPath()
    {
        if (this.router.url.indexOf('report/stockitem') >= 0)
        {
            this.checkSelectedReport("Stock Item");
        }else if (this.router.url.indexOf('report/incomingitem') >= 0)
        {
            this.checkSelectedReport("Incoming Item");
        }else if (this.router.url.indexOf('report/exititem') >= 0)
        {
            this.checkSelectedReport("Exit Item");
        }
    }

    checkSelectedReport(reportItem)
    {
        this.selectedReport = reportItem;
        this.reportTitle = {
            'Stock Item': 'Stok Barang',
            'Incoming Item': 'Barang Masuk',
            'Exit Item': 'Barang Keluar',
        }[this.selectedReport];
    }

    checkSession()
    {
        if (this.sessionService.retrieveSession())
        {
            this.userLoggedIn = true;
        }else
        {
            this.userLoggedIn = false;
        }
    }

    checkPrivilege()
    {
        this.privileges.viewReport = this.sessionService.checkPrivilege("View Report");
    }

    convertToExportData(item)
    {
        this.exportData = [];
        item.forEach(element => {
            let tempExportData={
                itemId: null,
                itemName:null,
                itemWeight : null,
                itemGoldContent: null,
                itemBuyPrice: null,
                itemBuyDate: null,
                categoryName: null,
                transactionPIC:null,
                transactionSoldDate:null,
                transactionSoldPrice:null,
                itemEntrustedStatus:null,
                itemNote:null
            };
            tempExportData.itemId = element.itemId;
            tempExportData.itemName = element.itemName;
            tempExportData.itemWeight = element.itemWeight;
            tempExportData.itemGoldContent = element.itemGoldContent;
            tempExportData.itemBuyPrice = element.itemBuyPrice;
            tempExportData.itemBuyDate = this.convertDate(element.itemBuyDate);
            tempExportData.categoryName = element.itemCategory.categoryName;
            if (element.itemTransaction)
            {
                tempExportData.transactionPIC = element.itemTransaction.transactionPIC;
                tempExportData.transactionSoldDate = this.convertDate(element.itemTransaction.transactionDate);
                tempExportData.transactionSoldPrice = element.itemTransaction.itemSoldPrice;
            }
            tempExportData.itemEntrustedStatus = element.itemEntrustedStatus;
            tempExportData.itemNote = element.itemNote;
            this.exportData.push(tempExportData);
        });
    }

    sendExport()
    {
        let fileName = this.selectedReport.replace(" ","") + this.selectedYear;
        if (this.checkedMonth)
        {
            fileName += this.pad(this.selectedMonth,2);
        }
        if (this.selectedReport == "Stock Item")
        {
            fileName += this.pad(this.selectedMonth,2);
        }
        if (this.entrustedOption == 0)
        {
            fileName += "Semua";
        }else if (this.entrustedOption == 1)
        {
            fileName += "BarangOrang";
        }else if (this.entrustedOption == 2)
        {
            fileName += "BarangSendiri";
        }
        fileName += ".csv"
        this.excelService.exportExcel(this.exportData,fileName);
    }

    pad(num:number, size:number): string {
        let s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }

    sendCancel()
    {
        this.router.navigate(['/admin/report/dashboard']);
        this.sharedService.notifyOther({ option: 'unhighlight', value: '' });
    }

    openErrorDialog(errorTitle, errorMessage)
    {
        let config = new MatDialogConfig();
        let dialogRef = this.dialog.open(ErrorDialogComponent, config);

        dialogRef.componentInstance.errorTitle = errorTitle;
        dialogRef.componentInstance.errorMessage = errorMessage;
    }

    openItemDetailDialog(item)
    {
      let config = new MatDialogConfig();
      let dialogRef = this.dialog.open(ItemViewDialogComponent, config);
      item.itemBuyDate = this.convertDate(item.itemBuyDate);
      dialogRef.componentInstance.item = item;  
      dialogRef.componentInstance.status = "Info";
    }

    openSoldDetailDialog(item)
    {
      let config = new MatDialogConfig();
      let dialogRef = this.dialog.open(SoldDetailDialogComponent, config);
      dialogRef.componentInstance.item = item;
    }

    getSoldIcon()
    {
        return `./assets/sold-icon.png`;
    }
  
}

export interface ExportData {
    itemId: null,
    itemName:null,
    itemWeight : null,
    itemGoldContent: null,
    itemBuyPrice: null,
    itemBuyDate: null,
    categoryName: null,
    transactionPIC:null,
    transactionSoldDate:null,
    transactionSoldPrice:null,
    itemEntrustedStatus:null,
    itemNote:null
}

export interface Item {
    itemId: String;
    itemName:String;
    itemWeight : null,
    itemGoldContent: null,
    itemBuyPrice: null,
    itemBuyDate: null,
    itemPhoto: null,
    itemCategory: {
      categoryId: null,
      categoryName: null
    },
    itemTransaction:null,
    itemEntrustedStatus:null,
    itemNote:null
}

/** An example database that the data source uses to retrieve data for the table. */
export class ItemRetrieve {
/** Stream that emits whenever the data has been modified. */
    dataChange: BehaviorSubject<Item[]> = new BehaviorSubject<Item[]>([]);
    get data(): Item[] { return this.dataChange.value; }

    constructor(private item:Item) {
    // Fill up the database with 100 users.
        this.addItem(item);  
    }

    addItem(item)
    {
        for (let i=0;i<item.length;i++)
        {
            const copiedData = this.data.slice();
            copiedData.push(item[i]);
            this.dataChange.next(copiedData);
        }
    }

    getLength()
    {
        return this.dataChange.value.length;
    }
}

/**
* Data source to provide what data should be rendered in the table. Note that the data source
* can retrieve its data in any way. In this case, the data source is provided a reference
* to a common data base, ExampleDatabase. It is not the data source's responsibility to manage
* the underlying data. Instead, it only needs to take the data and send the table exactly what
* should be rendered.
*/
export class ItemDataSource extends DataSource<any> {
    constructor(private itemRetrievedData: ItemRetrieve, private _paginator: MatPaginator) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<Item[]> {
        const displayDataChanges = [
            this.itemRetrievedData.dataChange,
            this._paginator.page
        ];
        return Observable.merge(...displayDataChanges).map(() => {
            const data = this.itemRetrievedData.data.slice();

            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        })
    }

    disconnect() {}
}