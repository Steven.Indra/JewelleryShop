import {Component, EventEmitter, Input, Output} from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { SharedService } from './../service/shared-service.service';
import { Subscription } from 'rxjs/Subscription';
import { SessionService } from 'app/service/session.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Input() loggedIn;

  @Output() logout = new EventEmitter();

  @Output() login = new EventEmitter();

  @Output() admin = new EventEmitter();

  @Output() changePassword = new EventEmitter();


  privileges = {
    adminDashboard: false
  };

  private subscription: Subscription;
  constructor(private sharedService: SharedService,
              private sessionService: SessionService) {}

  ngOnInit(){

    this.subscription = this.sharedService.notifyObservable$.subscribe((res) => {
        if (res.hasOwnProperty('option') && res.option === 'loginStatus' && res.value === "login") 
        {
          this.checkPrivilege();
        }else if (res.hasOwnProperty('option') && res.option === 'loginStatus' && res.value === "logout") 
        {
            this.checkPrivilege();
        }
    });
  }

  Admin(){
    this.admin.emit();
  }

  Login()
  {
    this.login.emit();
  }

  Logout()
  {
    this.logout.emit();
  }

  ChangePassword()
  {
    this.changePassword.emit();
  }

  checkPrivilege()
  {
    if (this.sessionService.checkPrivilege("Admin Dashboard"))
    {
      this.privileges.adminDashboard = false; // disable
    }else
    {
      this.privileges.adminDashboard = true;
    }
  }
}