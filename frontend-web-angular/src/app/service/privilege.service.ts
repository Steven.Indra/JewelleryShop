import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class PrivilegeService {
    constructor(private http: Http) {}

    getAll() {
        return this.http.get('http://localhost:8080/api/getAllPrivilege')
            .map(response => {
                return response.json();
            });
    }

    getAllNewUserPrivilege() {
        return this.http.get('http://localhost:8080/api/getAllNewUserPrivilege')
            .map(response => {
                return response.json();
            });
    }

    getPrivilegeInfo(privilegeId) {
        return this.http.get(`http://localhost:8080/api/getSinglePrivilege/${privilegeId}`)
            .map(response => {
                return response.json();
            });
    }

    private handleError (error: Response | any) 
    {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(`Error: ${errMsg}`);
        return Observable.throw(errMsg);
    }
}
