import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class TransactionService {
    constructor(private http: Http) {}

    getAll() {
        return this.http.get('http://localhost:8080/api/getTransactions')
            .map(response => {
                return response.json();
            });
    }

    getTransactionFiltered(filterQuery)
    {
        let entrustedValue="0";
        let PICValue="0";
        
        if (filterQuery.itemEntrustedStatus != false || filterQuery.itemEntrustedStatus ==null)
        {
            if (filterQuery.itemEntrustedStatus == null)
            {
                entrustedValue = '';
            }else
            {
                entrustedValue = filterQuery.itemEntrustedStatus;
            }
        }

        if (filterQuery.itemPIC != false || filterQuery.itemPIC ==null)
        {
            if (filterQuery.itemPIC == null)
            {
                PICValue = '';
            }else
            {
                PICValue = filterQuery.itemPIC;
            }
        }

        return this.http.get(`http://localhost:8080/api/filterTransaction?itemPIC=${PICValue}&itemEntrustedStatus=${entrustedValue}`)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    getReport(year,month,entrustedStatus)
    {
        return this.http.get(`http://localhost:8080/api/getTransactionReport?year=${year}&month=${month}&entrustedStatus=${entrustedStatus}`)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    getTransactionYear()
    {
        return this.http.get(`http://localhost:8080/api/getTransactionYear`)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    getTransactionInfo(transactionId) {
        return this.http.get(`http://localhost:8080/api/getSingleTransaction/${transactionId}`)
            .map(response => {
                return response.json();
            });
    }

    add(transaction) {
        return this.http.post('http://localhost:8080/api/modifyTransaction', transaction)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    edit(transaction) {
        return this.http.post(`http://localhost:8080/api/modifyTransaction`, transaction)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    delete(transactionId) {
        return this.http.delete(`http://localhost:8080/api/modifyTransaction/${transactionId}`)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    private handleError (error: Response | any) 
    {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(`Error: ${errMsg}`);
        return Observable.throw(errMsg);
    }
}
