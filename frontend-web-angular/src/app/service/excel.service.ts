import { Injectable } from '@angular/core';
import * as Papa from 'papaparse';


@Injectable()
export class ExcelService {
    constructor() {}

    exportExcel(JsonData,fileName) {
        var csvValue = this.unparseJsonToCSV(JsonData);
        var csvData = new Blob([csvValue], {type: 'text/csv;charset=utf-8;'});
        var csvURL = window.URL.createObjectURL(csvData);
        var tempLink = document.createElement('a');
        tempLink.href = csvURL;
        tempLink.setAttribute('download', fileName);
        tempLink.click();
    }

    unparseJsonToCSV(JsonData)
    {
        return Papa.unparse(JsonData);
    }

    parseCSVToJson(csvData)
    {
        return Papa.parse(csvData, {header : true});
    }

    importExcel(file, x: (data:any)=>void)
    {
        var data = null;
        var reader = new FileReader();
        
        reader.onload = (event: any) =>
        {
            var csvData = event.target.result;
            var data = this.parseCSVToJson(csvData);
            const arr = data.data.filter(x => x.itemId && x.itemName)
            x(arr);
        };

        reader.readAsText(file);
        reader.onerror = function() {
            alert('Unable to read ' + file.fileName);
        };
    }

    downloadImportFormatExcel() {
        var csvValue = "itemId,itemName,itemWeight,itemGoldContent,itemBuyPrice,itemBuyDate,categoryName,itemEntrustedStatus,itemNote"
        var csvData = new Blob([csvValue], {type: 'text/csv;charset=utf-8;'});
        var csvURL = window.URL.createObjectURL(csvData);
        var tempLink = document.createElement('a');
        tempLink.href = csvURL;
        tempLink.setAttribute('download', "ImportFormat.csv");
        tempLink.click();
    }
}
