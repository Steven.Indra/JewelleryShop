import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class UserService {
    constructor(private http: Http) {}

    getAll() {
        return this.http.get('http://localhost:8080/api/getAllUser')
            .map(response => {
                return response.json();
            });
    }

    getAllWithoutAdmin() {
        return this.http.get('http://localhost:8080/api/getAllUserNoAdmin')
            .map(response => {
                return response.json();
            });
    }

    getUserInfo(userId) {
        return this.http.get(`http://localhost:8080/api/getSingleUser/${userId}`)
            .map(response => {
                return response.json();
            });
    }

    tryLogin(username) {
        return this.http.get(`http://localhost:8080/api/tryLogin/${username}`)
            .map(response => {
                if (response.text() == "")
                {
                    return "wrong username";
                }else
                {
                    return response.json();
                }
                
            });
    }

    add(user) {
        return this.http.post('http://localhost:8080/api/modifyUser', user)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    edit(user, targetId) {
        user.append('userId', targetId);
        return this.http.post(`http://localhost:8080/api/modifyUser`, user)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    changePassword(user) {
        return this.http.post(`http://localhost:8080/api/changePassword`, user)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    delete(userId) {
        return this.http.delete(`http://localhost:8080/api/modifyUser/${userId}`)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    private handleError (error: Response | any) 
    {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(`Error: ${errMsg}`);
        return Observable.throw(errMsg);
    }
}
