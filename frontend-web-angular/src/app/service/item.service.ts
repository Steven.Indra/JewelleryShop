import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class ItemService {
    constructor(private http: Http) {}

    getItems() {
        return this.http.get('http://localhost:8080/api/getItems')
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    getItemsFiltered(filterQuery)
    {
        let soldValue=-1;
        let categoryValue=-1;
        let entrustedValue="0";
        
        if (filterQuery.itemSold)
        {
            soldValue = filterQuery.itemSold.id;
        }
        if (filterQuery.itemEntrustedStatus != false || filterQuery.itemEntrustedStatus ==null)
        {
            if (filterQuery.itemEntrustedStatus == null)
            {
                entrustedValue = '';
            }else
            {
                entrustedValue = filterQuery.itemEntrustedStatus;
            }
        }
        if (filterQuery.itemCategory)
        {
            categoryValue = filterQuery.itemCategory.categoryId;
        }

        return this.http.get(`http://localhost:8080/api/filterItem?itemCategoryId=${categoryValue}&itemEntrustedStatus=${entrustedValue}&itemSold=${soldValue}`)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    getReport(reportType,year,month,entrustedStatus)
    {
        return this.http.get(`http://localhost:8080/api/getReport?reportType=${reportType}&year=${year}&month=${month}&entrustedStatus=${entrustedStatus}`)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    getItemYear(reportType)
    {
        return this.http.get(`http://localhost:8080/api/getItemYear?reportType=${reportType}`)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    getItemsNotSold() {
        return this.http.get('http://localhost:8080/api/getItemsNotSold')
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    getItemInfo(id)
    {
        return this.http.get(`http://localhost:8080/api/getSingleItem/${id}`)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    add(item) {
        item.append('isAdd', 1);
        return this.http.post('http://localhost:8080/api/modifyItem', item)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    addImport(importData) {
        return this.http.post('http://localhost:8080/api/importBulkItem', importData)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    edit(item) {
        item.append('isAdd', 0);
        return this.http.post(`http://localhost:8080/api/modifyItem`, item)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    delete(item) {
        return this.http.delete(`http://localhost:8080/api/modifyItem/${item}`)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }
    
    private handleError (error: Response | any) 
    {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(`Error: ${errMsg}`);
        return Observable.throw(errMsg);
    }
}
