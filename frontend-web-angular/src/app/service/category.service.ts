import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class CategoryService {
    constructor(private http: Http) {}

    getAll() {
        return this.http.get('http://localhost:8080/api/getCategories')
            .map(response => {
                return response.json();
            });
    }

    getCategoryInfo(categoryId) {
        return this.http.get(`http://localhost:8080/api/getSingleCategory/${categoryId}`)
            .map(response => {
                return response.json();
            });
    }

    add(category) {
        return this.http.post('http://localhost:8080/api/modifyCategory', category)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    edit(category, targetId) {
        category.append('categoryId', targetId);
        return this.http.post(`http://localhost:8080/api/modifyCategory`, category)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    delete(categoryId) {
        return this.http.delete(`http://localhost:8080/api/modifyCategory/${categoryId}`)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    private handleError (error: Response | any) 
    {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(`Error: ${errMsg}`);
        return Observable.throw(errMsg);
    }
}
