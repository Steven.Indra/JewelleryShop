import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {
    userLoggedData;
    constructor() { }

    public createSession(userLoggedData) {
        this.userLoggedData = userLoggedData;
        localStorage.setItem('userLoggedIn', JSON.stringify(userLoggedData));
    }

    public renewSessionData(userLoggedData) {
        this.removeSession();
        this.createSession(userLoggedData);
    }

    public retrieveSession()
    {
        if (localStorage.getItem('userLoggedIn'))
        {
            this.userLoggedData = JSON.parse(localStorage.getItem('userLoggedIn'));
            return this.userLoggedData;
        }else
        {
            this.userLoggedData = "";
            return this.userLoggedData;
        }
    }

    public removeSession()
    {
        this.userLoggedData = "";
        localStorage.removeItem('userLoggedIn');
    }

    public checkPrivilege(privilegeName)
    {
        if (this.userLoggedData != undefined && this.userLoggedData != null && this.userLoggedData != "")
        {
            let getPrivilege = this.userLoggedData.privileges.filter(x=>x.privilegeName == privilegeName);
            if (getPrivilege.length>0)
            {
                return true;
            }else
            {
                return false;
            }
        }else
        {
            return false;
        }
    }
}