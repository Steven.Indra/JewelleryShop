For My Learning Purpose

Database:
- MySQL
- database name 'imsdb'
- File to import ./backend/imsdb19122017.sql

Backend:
- Spring-Boot
- REST
- JPA
- mvn spring-boot:run
- localhost:8080

Front-End Angular
- Angular 2 TS
- npm install
- npm start
- localhost:4200

Front-End Mobile React (In Progress) (Android only)
- React Native
- Android SDK build-tools 23.0.3
- npm install
- react-native run-android 