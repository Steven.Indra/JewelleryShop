import * as quoteActions from './quote';

export const ActionCreator = Object.assign({},
    quoteActions,
);