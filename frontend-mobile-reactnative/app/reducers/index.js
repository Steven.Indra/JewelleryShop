'use strict';

import { combineReducers } from 'redux';
import { quoteReducer } from './quote';

// Combine all the reducers
export default combineReducers({
    quoteReducer,
});