import React from 'react';
import Home from './components/home';
import NewQuote from './components/new_quote';
import Login from './components/login';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';



const DrawerStack = DrawerNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: ({navigation}) => ({
                title:'Home',
            }),
        },
        Admin: {
            screen: Home,
            navigationOptions: ({navigation}) => ({
                title:'Admin Dashboard'
            }),
        },
        Login: {
            screen: Login,
        },
    }, {
        navigationOptions: ({navigation}) => ({            
              title:  navigation.state.params ?  navigation.state.params.title : 'Home',
              headerRight: (
                <Icon name="angle-up" size={30} />
              ),
              headerLeft: (
                <Icon name="angle-down" size={30} onPress={()=> navigation.navigate('DrawerToggle')} />
              )
            }
        ),
    }
);

const RootStack = StackNavigator(
    {
        Drawer: {
            screen: DrawerStack,
            navigationOptions: ({navigation}) => ({
                headerLeft: (
                    <Icon name="angle-down" size={30} onPress={()=> navigation.navigate('DrawerToggle')} />
                )
            }),
        },
        Quote: {
            screen: NewQuote,
        },
    },
    {
        initialRouteName: 'Drawer',
        navigationOptions: ({navigation}) => ({
            headerStyle: {backgroundColor: '#4C3E54'},
            headerTintColor: 'white',
        }),
    }
);

DrawerStack.navigationOptions = props => {
    const { index, routes } = props.navigation.state.routes[0];
    const title = routes[index].routeName;
    return { title };
};

export default RootStack;