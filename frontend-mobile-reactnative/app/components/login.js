'use strict';

import React, {Component} from 'react';
var {
    StyleSheet,
    ListView,
    View,
    Text,
    TextInput,
    Image,
    ActivityIndicator, TouchableHighlight, ActionSheetIOS, TouchableWithoutFeedback, TouchableOpacity
} = require('react-native');

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';

import { ActionCreator } from '../actions'; //Import your actions

import {Actions} from 'react-native-router-flux';


//Buttons for Action Sheet
var BUTTONS = [
    "Edit",
    "Delete",
    'Cancel',
];

var CANCEL_INDEX = 2;

var _this;

class Login extends Component {
    constructor(props) {
        super(props);
    }

    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        
        return {
          title: 'Logout',
        }

    };

    componentDidMount() {
        this.props.getQuotes();
        _this = this;
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#eaeaea'}}>
                <Text style={{fontSize: 25, color: 'white'}}>asdasdasdas</Text>
            </View>
        )
    }
};


// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state
function mapStateToProps(state, props) {
    return {
        loading: state.quoteReducer.loading,
        quotes: state.quoteReducer.quotes,
        filter: state.quoteReducer.showFilter,
        searchQuery: state.quoteReducer.searchQuery
    }
}

// Doing this merges our actions into the component’s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreator, dispatch);
}

//Connect everything
export default connect(mapStateToProps, mapDispatchToProps)(Login);

var styles = StyleSheet.create({
    activityIndicatorContainer: {
        backgroundColor: "#fff",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },

    row: {
        backgroundColor: "#fff",
        padding: 8 * 2,
        marginBottom: 1,
        flexDirection: "row"
    },

    author: {
        fontSize: 14,
        fontWeight: "600",
    },

    quote: {
        fontSize: 14,
    },

    addButton: {
        backgroundColor: '#ff5722',
        borderColor: '#ff5722',
        borderWidth: 1,
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 20,
        right: 20,
        shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        }
    },

    container: {
        flex: 1,
        padding: 8,
        alignItems: 'center',
        backgroundColor: '#C1C1C1',
        flexDirection: 'row',
    },

    input: {
        height: 40,
        flex:8,
        paddingHorizontal: 8,
        fontSize: 15,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
        alignSelf: 'stretch',
    },

    photo: {
        height: 60,
        width: 60,
        borderRadius: 40,
    },
});