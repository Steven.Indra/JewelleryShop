'use strict';

import React, {Component} from 'react';
var {
    StyleSheet,
    ListView,
    View,
    Text,
    TextInput,
    Image,
    ActivityIndicator, TouchableHighlight, ActionSheetIOS, TouchableWithoutFeedback, TouchableOpacity
} = require('react-native');

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';

import { ActionCreator } from '../actions'; //Import your actions

import {Actions} from 'react-native-router-flux';


//Buttons for Action Sheet
var BUTTONS = [
    "Edit",
    "Delete",
    'Cancel',
];

var CANCEL_INDEX = 2;

var _this;

class Home extends Component {
    constructor(props) {
        super(props);

        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            ds: ds,
            showFilter: this.props.filter,
            searchQuery: this.props.searchQuery,
        };
    }

    // static navigationOptions = ({ navigation }) => {
    //     const { params } = navigation.state;
        
    //     return {
    //       title: params ? params.title : 'Home',
    //       headerRight: (
    //         <Icon name="angle-up" size={30} />
    //       ),
    //       headerLeft: (
    //         <Icon name="angle-down" size={30} onPress={()=> navigation.navigate('DrawerToggle')} />
    //       )
    //     }

    // };

    componentDidMount() {
        this.props.getQuotes();
        _this = this;
    }

    render() {
        if (this.props.loading) {
            return (
                <View style={styles.activityIndicatorContainer}>
                    <ActivityIndicator
                        animating={true}
                        style={[{height: 80}]}
                        size="small"
                    />
                </View>
            );
        } else {
            var myQuote = this.props.quotes;
            if (this.state.searchQuery != ""){
                myQuote = myQuote.filter(data => data.author.indexOf(this.state.searchQuery) >= 0);
            }
            return (
                
                <View style={{flex: 1, backgroundColor: '#eaeaea'}}>
                    <ListView enableEmptySections={true}
                              dataSource={this.state.ds.cloneWithRows(myQuote)}
                              renderRow={this.renderRow.bind(this)}
                              renderHeader={this.renderHeader.bind(this)}
                              />

                    <TouchableHighlight style={styles.addButton}
                                underlayColor='#ff7043' onPress={() => this.props.navigation.navigate('Quote', {edit: false, title:"Add Quote"})}>
                        <Text style={{fontSize: 25, color: 'white'}}>+</Text>
                    </TouchableHighlight>
                </View>
            );
        }
    }

    renderRow(rowData, sectionID, rowID) {
        return (
            // <TouchableWithoutFeedback onPress={() => this.toogleShow()}>
            <TouchableWithoutFeedback onPress={() => this.showDetails(rowData)}>
                <View style={styles.row}>
                    {/* <Avatar size={80} image={<Image source={{ uri: "http://facebook.github.io/react-native/img/opengraph.png?2" }}/>} /> */}
                    <Image source={{ uri: "http://facebook.github.io/react-native/img/opengraph.png?2" }} style={styles.photo} />
                    <View style={{flex:1, paddingLeft:10}}>
                        <Text style={styles.author}>
                            {rowData.author}
                        </Text>
                        <Text numberOfLines={2} style={styles.description}>
                            {rowData.quote}
                        </Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    renderHeader(){
        return (
            <View>
                <View style={styles.container}>
                    <TextInput
                        style={styles.input}
                        placeholder="Search..."
                        onChangeText={(text) => this.setState({searchQuery: text})}
                    />
                    <TouchableWithoutFeedback style={{flex:1}} onPress={() => this.toogleShow()}>
                        <View style={{flex:1,alignSelf: 'stretch',alignItems: 'center',justifyContent: 'center'}}>
                            {this.renderFilterIcon()}
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                {this.renderFilter()}
            </View>
        )
    }

    renderFilter(){
        if (this.state.showFilter) {
            return (
                <View>
                    <Text>tes2</Text>
                    <Text>tes2</Text>
                    <Text>tes2</Text>
                    <Text>tes2</Text>
                    <Text>tes2</Text>
                    <Text>tes2</Text>
                </View>
            );
        } else {
            return null;
        }
    }

    renderFilterIcon(){
        if (this.state.showFilter) {
            return (
                <Icon name="angle-up" size={30} />
            );
        } else {
            return (
                <Icon name="angle-down" size={30} />
            );
        }
    }

    toogleShow(){
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    showDetails(quote) {
        this.props.navigation.navigate('Quote', {quote: quote, edit: true, title:"Edit Quote"});
    }
};


// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state
function mapStateToProps(state, props) {
    return {
        loading: state.quoteReducer.loading,
        quotes: state.quoteReducer.quotes,
        filter: state.quoteReducer.showFilter,
        searchQuery: state.quoteReducer.searchQuery
    }
}

// Doing this merges our actions into the component’s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreator, dispatch);
}

//Connect everything
export default connect(mapStateToProps, mapDispatchToProps)(Home);

var styles = StyleSheet.create({
    activityIndicatorContainer: {
        backgroundColor: "#fff",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },

    row: {
        backgroundColor: "#fff",
        padding: 8 * 2,
        marginBottom: 1,
        flexDirection: "row"
    },

    author: {
        fontSize: 14,
        fontWeight: "600",
    },

    quote: {
        fontSize: 14,
    },

    addButton: {
        backgroundColor: '#ff5722',
        borderColor: '#ff5722',
        borderWidth: 1,
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 20,
        right: 20,
        shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        }
    },

    container: {
        flex: 1,
        padding: 8,
        alignItems: 'center',
        backgroundColor: '#C1C1C1',
        flexDirection: 'row',
    },

    input: {
        height: 40,
        flex:8,
        paddingHorizontal: 8,
        fontSize: 15,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
        alignSelf: 'stretch',
    },

    photo: {
        height: 60,
        width: 60,
        borderRadius: 40,
    },
});